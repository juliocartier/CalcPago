import { Component, ViewChild } from '@angular/core';

import { Platform, MenuController, Nav } from 'ionic-angular';

import { HelloIonicPage } from '../pages/hello-ionic/hello-ionic';
import { ItemDetailsPage } from '../pages/item-details/item-details';
import { MercadoPagoPage } from '../pages/mercado-pago/mercado-pago';
import { CadastroPage } from '../pages/cadastro/cadastro';
import { UpgradePage } from '../pages/upgrade/upgrade';
import { ConfiguracaoPage} from '../pages/configuracao/configuracao';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  // make HelloIonicPage the root (or first) page
  rootPage = MercadoPagoPage;
  pages: Array<{title: string, component: any}>;
  pages2 : any;

  constructor(
    public platform: Platform,
    public menu: MenuController,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen
  ) {
    this.initializeApp();

    // set our app's pages
    /*this.pages = [
      { title: 'Mercado Pago', component: MercadoPagoPage },
      { title: 'Ajuda', component: ItemDetailsPage },
      { title: 'Sobre', component: HelloIonicPage},
      { title: 'Cadastrar Produto', component: CadastroPage},
      { title: 'Fazer Upgrade!', component: UpgradePage },
    ];*/

    this.pages2 = {
     MercadoPagoPage: MercadoPagoPage,
     ItemDetailsPage: ItemDetailsPage,
     HelloIonicPage: HelloIonicPage,
     CadastroPage: CadastroPage,
     UpgradePage: UpgradePage,
     ConfiguracaoPage: ConfiguracaoPage
    }
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    this.nav.setRoot(page.component);
  }
}
