import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { HelloIonicPage } from '../pages/hello-ionic/hello-ionic';
import { ItemDetailsPage } from '../pages/item-details/item-details';
import { MercadoPagoPageModule} from '../pages/mercado-pago/mercado-pago.module';
import { CadastroPageModule} from '../pages/cadastro/cadastro.module';
import { AdicionaPageModule} from '../pages/adiciona/adiciona.module';
import { EditarPageModule} from '../pages/editar/editar.module';
import { ConfiguracaoPageModule } from '../pages/configuracao/configuracao.module';
import { ResultadoCartaoPageModule} from '../pages/resultado-cartao/resultado-cartao.module';
import { UpgradePageModule } from '../pages/upgrade/upgrade.module';
import { Screenshot } from '@ionic-native/screenshot';
import { Market } from '@ionic-native/market';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AdMobFree } from '@ionic-native/admob-free';
import { SQLite } from '@ionic-native/sqlite';
import { Toast } from '@ionic-native/toast';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    HelloIonicPage,
    ItemDetailsPage,
    //ConfiguracaoPageModule
    //ModalBasicPage,
    //ModalContentPage,
  ],
  imports: [
    BrowserModule,
    MercadoPagoPageModule,
    CadastroPageModule,
    AdicionaPageModule,
    EditarPageModule,
    ResultadoCartaoPageModule,
    UpgradePageModule,
    ConfiguracaoPageModule,
    //ModalBasicPage,
    //ModalContentPage,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    HttpModule,
    TranslateModule.forRoot({
      loader : {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HelloIonicPage,
    ItemDetailsPage,
    //ConfiguracaoPageModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Screenshot,
    AdMobFree,
    Market,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    SQLite,
    Toast                       
  ]
})
export class AppModule {}
