import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditarPage } from './editar';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    EditarPage,
  ],
  imports: [
    TranslateModule,
    IonicPageModule.forChild(EditarPage),
  ],
})
export class EditarPageModule {}
