import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Market } from '@ionic-native/market';
import { AlertController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

/**
 * Generated class for the UpgradePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-upgrade',
  templateUrl: 'upgrade.html',
})
export class UpgradePage {
  a: any = {};

  constructor(public navCtrl: NavController, public navParams: NavParams, private market: Market, public alertCtrl: AlertController, public translate: TranslateService) {
    this.funcaoAppPro();
  }

  funcaoAppPro(){

    this.translate.get('Versão PRO').subscribe(t => {
      this.a.title = t;
    });

    this.translate.get('Clique em Ok para a Versão PRO').subscribe(t => {
      this.a.subTitle = t;
    });

    let alert = this.alertCtrl.create({
      title: this.a.title,
      subTitle: this.a.subTitle,
      buttons: ['Ok']
    });
    alert.present();
    
    this.market.open('calc.tarifas.mercadoPagoPro');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UpgradePage');
  }

}
