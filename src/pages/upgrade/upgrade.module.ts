import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UpgradePage } from './upgrade';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    UpgradePage,
  ],
  imports: [
    TranslateModule,
    IonicPageModule.forChild(UpgradePage),
  ],
})
export class UpgradePageModule {}
