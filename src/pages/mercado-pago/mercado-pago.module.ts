import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MercadoPagoPage } from './mercado-pago';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    MercadoPagoPage,
  ],
  imports: [
    TranslateModule,
    IonicPageModule.forChild(MercadoPagoPage),
  ],
})
export class MercadoPagoPageModule {}
