import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, FabContainer } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
//import { AdMobFree, AdMobFreeInterstitialConfig} from '@ionic-native/admob-free';

/**
 * Generated class for the MercadoPagoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mercado-pago',
  templateUrl: 'mercado-pago.html'
})
export class MercadoPagoPage {  
  public objetos_pagamento : any;
  public objeto_pag : any;
  public objeto_parcelamento : any;

	calculoTaxas : string = '';
	parcelas : any;
	maquina : string = '';
	pagamento : string = '';
	recebimento : string = '';
	parcelamento : string = '';
  valorDig : any;
  plano : any;
  lang : any;
  a: any = {};

  /*debito: any = this.navParams.get('debito');
  creditoHora: any = this.navParams.get('creditoHora');
  credito14: any = this.navParams.get('credito14');
  credito30: any = this.navParams.get('credito30');
  creditoParceladoHora: any = this.navParams.get('creditoParceladoHora');
  creditoParcelado14: any = this.navParams.get('creditoParcelado14');
  creditoParcelado30: any = this.navParams.get('creditoParcelado30');
  parcela2: any = this.navParams.get('parcela2');
  parcela3: any = this.navParams.get('parcela3');
  parcela4: any = this.navParams.get('parcela4');
  parcela5: any = this.navParams.get('parcela5');
  parcela6: any = this.navParams.get('parcela6');
  parcela7: any = this.navParams.get('parcela7');
  parcela8: any = this.navParams.get('parcela8');
  parcela9: any = this.navParams.get('parcela9');
  parcela10: any = this.navParams.get('parcela10');
  parcela11: any = this.navParams.get('parcela11');
  parcela12: any = this.navParams.get('parcela12');*/

  /** 
   * Variaveis para calculo
  */

  valorAux : any;
  valorParcela : any;
  porcentagemVendedor: any;
  totalImp: any;
  totalParcelamento: any;
  totalTaxas : any;
  totalPorVendedor : any;
  total: any;
  totalCliente: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, private modal: ModalController, public translate: TranslateService) {
    this.lang = 'pt';
    this.translate.setDefaultLang('pt');
    this.translate.use('pt');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MercadoPagoPage');
  }

  traducao(lang: any, fab: FabContainer) {
    fab.close();
    //console.log("Sharing in", lang);
    this.translate.use(lang);
  }

  onSelectChange(selectedValue: any) {
    this.plano = selectedValue;
    if (this.plano == 'Debito') {
       this.objetos_pagamento = [{recebimento: 'Hora'}];
       this.objeto_pag = [{parcelas: 1}];
       this.objeto_parcelamento = [{parcelamento:'Vendedor'}];
      } else if(this.plano == 'Credito'){
        this.objetos_pagamento = [{recebimento: 'Hora'}, {recebimento: '14'},{recebimento: '30'}];
        this.objeto_pag = [{parcelas : 1},{parcelas: 2},{parcelas: 3},{parcelas: 4}
       ,{parcelas: 5}, {parcelas: 6}, {parcelas: 7}, {parcelas: 8}, {parcelas: 9},
       {parcelas: 10}, {parcelas: 11}, {parcelas: 12}];
       this.objeto_parcelamento = [{parcelamento:'Vendedor'},{parcelamento:'Cliente'}];
      } 
  } 

openModal(valorDig : any){

    let calculoTaxas = this.calculoTaxas;
    let parcelas = parseFloat(this.parcelas);
    let maquina = this.maquina;
    let pagamento = this.pagamento;
    let recebimento = this.recebimento;
    let parcelamento = this.parcelamento;
    let valor = parseFloat(this.valorDig);
    let lang = this.lang;
    /*let debito = this.debito;
    let creditoHora = this.creditoHora;
    let credito14 = this.credito14;
    let credito30 = this.credito30;
    let creditoParceladoHora = this.creditoParceladoHora;
    let creditoParcelado14 = this.creditoParcelado14;
    let creditoParcelado30 = this.creditoParcelado30;
    let parcela2 = this.parcela2;
    let parcela3 = this.parcela3;
    let parcela4 = this.parcela4;
    let parcela5 = this.parcela5;
    let parcela6 = this.parcela6;
    let parcela7 = this.parcela7;
    let parcela8 = this.parcela8;
    let parcela9 = this.parcela9;
    let parcela10 = this.parcela10;
    let parcela11 = this.parcela11;
    let parcela12 = this.parcela12;

  if (this.debito == undefined || this.creditoHora == undefined || this.credito14 == undefined || this.credito30 == undefined || this.creditoParceladoHora == undefined
    || this.creditoParcelado14 == undefined || this.creditoParcelado30 == undefined || this.parcela2 == undefined || this.parcela3 == undefined || this.parcela4 == undefined
  || this.parcela5 == undefined || this.parcela6 == undefined || this.parcela7 == undefined || this.parcela8 == undefined || this.parcela9 == undefined || this.parcela10 == undefined
  || this.parcela11 == undefined || this.parcela12 == undefined){
    debito = 2.29;
    creditoHora = 4.74;
    credito14 = 3.79;
    credito30 = 3.03;
    creditoParceladoHora = 5.31;
    creditoParcelado14 = 4.36;
    creditoParcelado30 = 3.60;
    parcela2 = 4.09;
    parcela3 = 5.41;
    parcela4 = 6.70;
    parcela5 = 7.96;
    parcela6 = 9.20;
    parcela7 = 10.41;
    parcela8 = 11.61;
    parcela9 = 12.78;
    parcela10 = 13.92;
    parcela11 = 15.05;
    parcela12 = 16.15;
  } else if (this.debito != undefined) {
    debito = this.debito;
  }  else if (this.creditoHora != undefined) {
    creditoHora = this.creditoHora;
  } else if (this.credito14 != undefined){
    credito14 = this.credito14;
  } else if (this.credito30 != undefined){
    credito30 = this.credito30;
  } else if (this.creditoParceladoHora != undefined){
    creditoParceladoHora = this.creditoParceladoHora;
  } else if (this.creditoParcelado14 != undefined){
    creditoParcelado14 = this.creditoParcelado14;
  } else if (this.creditoParcelado30 != undefined){
    creditoParcelado30 = this.creditoParcelado30;
  } else if (this.parcela2 != undefined){
    parcela2 = this.parcela2;
  } else if (this.parcela3 != undefined){
    parcela3 = this.parcela3;
  } else if (this.parcela4 != undefined){
    parcela4 = this.parcela4;
  } else if (this.parcela5 != undefined){
    parcela5 = this.parcela5;
  } else if (this.parcela6 != undefined){
    parcela6 = this.parcela6;
  } else if (this.parcela7 != undefined) {
    parcela7 = this.parcela7;
  } else if (this.parcela8 != undefined) {
    parcela8 = this.parcela8;
  } else if (this.parcela9 != undefined) {
    parcela9 = this.parcela9;
  } else if (this.parcela10 != undefined) {
    parcela10 = this.parcela10;
  } else if (this.parcela11 != undefined) {
    parcela11 = this.parcela11;
  } else if (this.parcela12 != undefined) {
    parcela12 = this.parcela12;
  }*/

if (calculoTaxas == "" || parcelas == 0 || maquina == "" || pagamento == "" || recebimento == "" || parcelamento == "" || valor <= 0) {
       
  this.translate.get('Atenção!').subscribe(t => {
    this.a.title = t;
  });

  this.translate.get('Por favor, preencha todos os campos.').subscribe(t => {
    this.a.subTitle = t;
  });
  
        let alert = this.alertCtrl.create({
            title: this.a.title,
            subTitle: this.a.subTitle,
            buttons: ['Ok']
          });
          alert.present();
        
} else {
  let myData = {
      calculoTaxas: calculoTaxas,
      valor: valor,
      maquina: maquina,
      pagamento: pagamento,
      parcelas: parcelas,
      recebimento: recebimento,
      parcelamento: parcelamento,
      lang : lang,
      /*debito : debito,
      creditoHora : creditoHora,
      credito14 : credito14,
      credito30 : credito30,
      creditoParceladoHora: creditoParceladoHora,
      creditoParcelado14 : creditoParcelado14,
      creditoParcelado30 : creditoParcelado30,
      parcela2 : parcela2,
      parcela3: parcela3,
      parcela4: parcela4,
      parcela5: parcela5,
      parcela6: parcela6,
      parcela7: parcela7,
      parcela8: parcela8,
      parcela9: parcela9,
      parcela10: parcela10,
      parcela11: parcela11,
      parcela12: parcela12*/
    }; 

    console.log(myData);
    const myModal = this.modal.create('ModalPage',  myData);

    myModal.present();
}

 
  }

  taxas(){
    //this.valorDig = this.navParams.get('valorDig');
    //console.log("Esta funcionando o valor" + this.valorDig);
    if (this.recebimento == "Hora") {
      if(this.pagamento == "Debito"){
        if(this.parcelas == 1){
          this.valorParcela = (this.valorDig/1);
          this.totalImp = this.valorDig * (1.99/100);
          this.totalParcelamento = this.valorDig * 0;
          this.totalTaxas = this.totalImp + this.totalParcelamento;
          this.total = this.valorDig - this.totalTaxas;
          this.totalCliente = this.valorDig;
        }
      } else if (this.pagamento == "Credito") { // Inicio do if do tipo credito
        if (this.parcelas == 1) { //Inicio do if da quantidade de parcelas = 1 
          //Calculos
          this.valorAux = this.valorDig;
          this.valorParcela = (this.valorAux / this.parcelas);
          this.totalPorVendedor = this.porcentagemVendedor;
          this.totalParcelamento = this.valorDig * 0;
          this.totalTaxas = this.totalImp + this.totalParcelamento;
          this.total = this.valorDig - this.totalTaxas;

          //Calculos
          this.valorAux = this.valorDig;
          this.valorParcela = (this.valorAux / this.parcelas);
          this.totalPorVendedor = this.porcentagemVendedor;
          this.totalTaxas = this.totalImp + this.totalParcelamento;
          this.total = this.valorDig - this.totalTaxas;
          this.totalCliente = this.valorDig;
        } // fim do if do cliente

      }// fim do if da quantidade de parcelas = 1
    } 
  }

  reverso(){
    if (this.recebimento == "Hora") {
      if(this.pagamento == "Debito"){
        if(this.parcelas == 1 || this.parcelas == undefined){
          this.totalImp = this.valorDig * (0.0199 + 0.0006);
          this.total =  parseFloat(this.valorDig) + parseFloat(this.totalImp);
          this.totalCliente = (parseFloat(this.total) - (parseFloat(this.total) * 0.0199)).toFixed(2);
        }
      }
    }

    
  }

}

