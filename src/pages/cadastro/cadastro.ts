import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { AdicionaPage } from '../adiciona/adiciona';
import { EditarPage } from '../editar/editar';
import { AdMobFree, AdMobFreeInterstitialConfig, AdMobFreeBannerConfig} from '@ionic-native/admob-free';

/**
 * Generated class for the CadastroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cadastro',
  templateUrl: 'cadastro.html',
})
export class CadastroPage {
	expenses: any = [];
	totalIncome = 0;
	totalExpense = 0;
	balance = 0;
	//valorTotal : any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private sqlite: SQLite, private admobFree: AdMobFree) {
  	this.showInterstialAd();
    this.showBannerAd();
    this.showVideoAd();
  }

  ionViewDidLoad() {
    this.getData();
  }

  ionViewWillEnter() {
  	this.getData();
	}

getData() {
  this.sqlite.create({
    name: 'cadastro.db',
    location: 'default'
  }).then((db: SQLiteObject) => {
    db.executeSql('CREATE TABLE IF NOT EXISTS expense(rowid INTEGER PRIMARY KEY, date TEXT, nome TEXT, valor INT)', {})
    .then(res => console.log('Executed SQL'))
    .catch(e => console.log(e));
    db.executeSql('SELECT * FROM expense ORDER BY rowid DESC', {})
    .then(res => {
      this.expenses = [];
      for(var i=0; i<res.rows.length; i++) {
        this.expenses.push({rowid:res.rows.item(i).rowid,
        	date:res.rows.item(i).date,
        	nome:res.rows.item(i).nome,
        	valor:res.rows.item(i).valor
        })
        
        /*if (res.rows.item(i).operacao == "Taxas") {
        	this.expenses.push({valorTotal:parseInt(res.rows.item(i).valor) - (parseInt(res.rows.item(i).valor) * 0.0229)})
        } else if (res.rows.item(i).operacao == "Reverso") {
        	this.expenses.push({valorTotal:parseInt(res.rows.item(i).valor) + (parseInt(res.rows.item(i).valor) * 0.02345)})
        }*/

      }
    })
    .catch(e => console.log(e));
    db.executeSql('SELECT SUM(valor) AS totalIncome FROM expense WHERE type="Income"', {})
    .then(res => {
      if(res.rows.length>0) {
        this.totalIncome = parseInt(res.rows.item(0).totalIncome);
        this.balance = this.totalIncome-this.totalExpense;
      }
    })
    .catch(e => console.log(e));
    db.executeSql('SELECT SUM(valor) AS totalExpense FROM expense WHERE type="Expense"', {})
    .then(res => {
      if(res.rows.length>0) {
        this.totalExpense = parseInt(res.rows.item(0).totalExpense);
        this.balance = this.totalIncome-this.totalExpense;
      }
    })
  }).catch(e => console.log(e));
}

addData() {
  this.navCtrl.push(AdicionaPage);
}

editData(rowid) {
  this.navCtrl.push(EditarPage, {
    rowid:rowid
  });
}

deleteData(rowid) {
  this.sqlite.create({
    name: 'cadastro.db',
    location: 'default'
  }).then((db: SQLiteObject) => {
    db.executeSql('DELETE FROM expense WHERE rowid=?', [rowid])
    .then(res => {
      console.log(res);
      this.getData();
    })
    .catch(e => console.log(e));
  }).catch(e => console.log(e));
}

    async showInterstialAd(){
    try {
      const interstitialConfig: AdMobFreeInterstitialConfig = {
        id: 'ca-app-pub-4250164210737037/5911486633',
        isTesting: false,
        autoShow: true
      }

      this.admobFree.interstitial.config(interstitialConfig);

      const result = await this.admobFree.interstitial.prepare();
      //console.log("Esta funcionando", result);

    } catch(e) {
      // statements
      console.log(e);
    }
  }

  async showBannerAd(){
    const bannerConfig: AdMobFreeBannerConfig = {
      id: 'ca-app-pub-4250164210737037/4131123805',
      isTesting: false,
      autoShow: true
      //bannerAtTop: true
    }

    this.admobFree.banner.config(bannerConfig);

    try {
      const result = this.admobFree.banner.prepare();
    } catch(e) {
      // statements
      //console.log(e);
      console.error(e);
    }
    
  }

    async showVideoAd(){
      try {
      const interstitialConfig: AdMobFreeInterstitialConfig = {
        id: 'ca-app-pub-4250164210737037/5911486633',
        isTesting: false,
        autoShow: true
      }

      this.admobFree.interstitial.config(interstitialConfig);

      const result = await this.admobFree.interstitial.prepare();
      //console.log("Esta funcionando", result);

    } catch(e) {
      // statements
      console.log(e);
    }
    
  }

}
