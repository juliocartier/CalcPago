import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResultadoCartaoPage } from './resultado-cartao';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    ResultadoCartaoPage,
  ],
  imports: [
    TranslateModule,
    IonicPageModule.forChild(ResultadoCartaoPage),
  ],
})
export class ResultadoCartaoPageModule {}
