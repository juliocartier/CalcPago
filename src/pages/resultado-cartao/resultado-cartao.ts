import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AdMobFree, AdMobFreeInterstitialConfig, AdMobFreeBannerConfig} from '@ionic-native/admob-free';
import { TranslateService } from '@ngx-translate/core';

/**
 * Generated class for the ResultadoCartaoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-resultado-cartao',
  templateUrl: 'resultado-cartao.html',
})
export class ResultadoCartaoPage {

	tipoModo : any = this.navParams.get('calculoTaxas');
	valorTotal : any = this.navParams.get('valor');
	recebimento : any = this.navParams.get('recebimento');
	parcelamento : any = this.navParams.get('parcelamento');
	lang: string = this.navParams.get('lang');
	debito: any = this.navParams.get('debito');
	creditoHora: any = this.navParams.get('creditoHora');
	credito14: any = this.navParams.get('credito14');
	credito30: any = this.navParams.get('credito30');
	creditoParceladoHora: any = this.navParams.get('creditoParceladoHora');
	creditoParcelado14: any = this.navParams.get('creditoParcelado14');
	creditoParcelado30: any = this.navParams.get('creditoParcelado30');
	parcela2: any = this.navParams.get('parcela2');
	parcela3: any = this.navParams.get('parcela3');
	parcela4: any = this.navParams.get('parcela4');
	parcela5: any = this.navParams.get('parcela5');
	parcela6: any = this.navParams.get('parcela6');
	parcela7: any = this.navParams.get('parcela7');
	parcela8: any = this.navParams.get('parcela8');
	parcela9: any = this.navParams.get('parcela9');
	parcela10: any = this.navParams.get('parcela10');
	parcela11: any = this.navParams.get('parcela11');
	parcela12: any = this.navParams.get('parcela12');
	valorDebito : any;
	valorParcela1 : any;
	valorParcela2 : any;
	valorParcela3 : any;
	valorParcela4 : any;
	valorParcela5 : any;
	valorParcela6 : any;
	valorParcela7 : any;
	valorParcela8 : any;
	valorParcela9 : any;
	valorParcela10 : any;
	valorParcela11 : any;
	valorParcela12 : any;
	textoTaxas : any;

	constructor(public navCtrl: NavController, public navParams: NavParams, private admobFree: AdMobFree, public translate: TranslateService) {
  	this.showVideoAd();
    this.showBannerAd();
    this.showInterstialAd();
  	this.funcaoPagamentoDetalhado();
	}
	
	switchLanguage() {
		this.translate.use(this.lang);
	}

      async showInterstialAd(){
    try {
      const interstitialConfig: AdMobFreeInterstitialConfig = {
        id: 'ca-app-pub-4250164210737037/5911486633',
        isTesting: false,
        autoShow: true
      }

      this.admobFree.interstitial.config(interstitialConfig);

      const result = await this.admobFree.interstitial.prepare();
      console.log("Esta funcionando", result);

    } catch(e) {
      // statements
      console.log(e);
    }
  }

  async showBannerAd(){
    const bannerConfig: AdMobFreeBannerConfig = {
      id: 'ca-app-pub-4250164210737037/4131123805',
      isTesting: false,
      autoShow: true
      //bannerAtTop: true
    }

    this.admobFree.banner.config(bannerConfig);

    try {
      const result = this.admobFree.banner.prepare();
    } catch(e) {
      // statements
      //console.log(e);
      console.error(e);
    }
    
  }

    async showVideoAd(){
    try {
      const interstitialConfig: AdMobFreeInterstitialConfig = {
        id: 'ca-app-pub-4250164210737037/5911486633',
        isTesting: false,
        autoShow: true
      }

      this.admobFree.interstitial.config(interstitialConfig);

      const result = await this.admobFree.interstitial.prepare();
      //console.log("Esta funcionando", result);

    } catch(e) {
      // statements
      console.log(e);
    }
  }


  funcaoPagamentoDetalhado(){
  	let tipo = this.tipoModo;
    let valor = this.valorTotal;
    let recebimento = this.recebimento;
    let parcelamento = this.parcelamento;


    if (tipo == "Taxas") {
    	if (recebimento == "Hora") {
    		if (parcelamento == "Vendedor") {
    			 //parseFloat("" + (0.0378 * 100).toFixed(2))
    			this.textoTaxas = "Você irá receber no:";
				this.valorDebito = parseFloat("" + (valor - (valor * (this.debito / 100))).toFixed(2));
				this.valorParcela1 = parseFloat("" + (valor - (valor * (this.creditoHora / 100))).toFixed(2));
				this.valorParcela2 = parseFloat("" + (valor - (valor * (this.creditoParceladoHora / 100)) - (valor * (this.parcela2 / 100))).toFixed(2));
				this.valorParcela3 = parseFloat("" + (valor - (valor * (this.creditoParceladoHora / 100)) - (valor * (this.parcela3 / 100))).toFixed(2));
				this.valorParcela4 = parseFloat("" + (valor - (valor * (this.creditoParceladoHora / 100)) - (valor * (this.parcela4 / 100))).toFixed(2));
				this.valorParcela5 = parseFloat("" + (valor - (valor * (this.creditoParceladoHora / 100)) - (valor * (this.parcela5 / 100))).toFixed(2));
				this.valorParcela6 = parseFloat("" + (valor - (valor * (this.creditoParceladoHora / 100)) - (valor * (this.parcela6 / 100))).toFixed(2));
				this.valorParcela7 = parseFloat("" + (valor - (valor * (this.creditoParceladoHora / 100)) - (valor * (this.parcela7 / 100))).toFixed(2));
				this.valorParcela8 = parseFloat("" + (valor - (valor * (this.creditoParceladoHora / 100)) - (valor * (this.parcela8 / 100))).toFixed(2));
				this.valorParcela9 = parseFloat("" + (valor - (valor * (this.creditoParceladoHora / 100)) - (valor * (this.parcela9 / 100))).toFixed(2));
				this.valorParcela10 = parseFloat("" + (valor - (valor * (this.creditoParceladoHora / 100)) - (valor * (this.parcela10 / 100))).toFixed(2));
				this.valorParcela11 = parseFloat("" + (valor - (valor * (this.creditoParceladoHora / 100)) - (valor * (this.parcela11 / 100))).toFixed(2));
				this.valorParcela12 = parseFloat("" + (valor - (valor * (this.creditoParceladoHora / 100)) - (valor * (this.parcela12 / 100))).toFixed(2));
		

    		}else if(parcelamento == "Cliente"){
    			this.textoTaxas = "Você irá receber no:";
				this.valorDebito = parseFloat("" + (valor - (valor * (this.debito / 100))).toFixed(2)); 
				this.valorParcela1 = parseFloat("" + (valor - (valor * (this.creditoHora / 100))).toFixed(2));
		    	this.valorParcela2 = parseFloat("" + (valor - (valor * (this.creditoParceladoHora / 100))).toFixed(2));
		    	this.valorParcela3 = parseFloat("" + (valor - (valor * (this.creditoParceladoHora / 100))).toFixed(2));
		    	this.valorParcela4 = parseFloat("" + (valor - (valor * (this.creditoParceladoHora / 100))).toFixed(2));
		    	this.valorParcela5 = parseFloat("" + (valor - (valor * (this.creditoParceladoHora / 100))).toFixed(2));
		    	this.valorParcela6 = parseFloat("" + (valor - (valor * (this.creditoParceladoHora / 100))).toFixed(2));
		    	this.valorParcela7 = parseFloat("" + (valor - (valor * (this.creditoParceladoHora / 100))).toFixed(2));
		    	this.valorParcela8 = parseFloat("" + (valor - (valor * (this.creditoParceladoHora / 100))).toFixed(2));
		    	this.valorParcela9 = parseFloat("" + (valor - (valor * (this.creditoParceladoHora / 100))).toFixed(2));
		    	this.valorParcela10 = parseFloat("" + (valor - (valor * (this.creditoParceladoHora / 100))).toFixed(2));
		    	this.valorParcela11 = parseFloat("" + (valor - (valor * (this.creditoParceladoHora / 100))).toFixed(2));
		    	this.valorParcela12 = parseFloat("" + (valor - (valor * (this.creditoParceladoHora / 100))).toFixed(2));
    		}
    	} 
    	else if (recebimento == "14") {
    		if (parcelamento == "Vendedor") {
    			this.textoTaxas = "Você irá receber no:";
				this.valorDebito = parseFloat("" + (valor - (valor * (this.debito / 100))).toFixed(2));
				this.valorParcela1 = parseFloat("" + (valor - (valor * (this.credito14 / 100))).toFixed(2));
				this.valorParcela2 = parseFloat("" + (valor - (valor * (this.creditoParcelado14 / 100)) - (valor * (this.parcela2 / 100))).toFixed(2));
				this.valorParcela3 = parseFloat("" + (valor - (valor * (this.creditoParcelado14 / 100)) - (valor * (this.parcela3 / 100))).toFixed(2));
				this.valorParcela4 = parseFloat("" + (valor - (valor * (this.creditoParcelado14 / 100)) - (valor * (this.parcela4 / 100))).toFixed(2));
				this.valorParcela5 = parseFloat("" + (valor - (valor * (this.creditoParcelado14 / 100)) - (valor * (this.parcela5 / 100))).toFixed(2));
				this.valorParcela6 = parseFloat("" + (valor - (valor * (this.creditoParcelado14 / 100)) - (valor * (this.parcela6 / 100))).toFixed(2));
				this.valorParcela7 = parseFloat("" + (valor - (valor * (this.creditoParcelado14 / 100)) - (valor * (this.parcela7 / 100))).toFixed(2));
				this.valorParcela8 = parseFloat("" + (valor - (valor * (this.creditoParcelado14 / 100)) - (valor * (this.parcela8 / 100))).toFixed(2));
				this.valorParcela9 = parseFloat("" + (valor - (valor * (this.creditoParcelado14 / 100)) - (valor * (this.parcela9 / 100))).toFixed(2));
				this.valorParcela10 = parseFloat("" + (valor - (valor * (this.creditoParcelado14 / 100)) - (valor * (this.parcela10 / 100))).toFixed(2));
				this.valorParcela11 = parseFloat("" + (valor - (valor * (this.creditoParcelado14 / 100)) - (valor * (this.parcela11 / 100))).toFixed(2));
				this.valorParcela12 = parseFloat("" + (valor - (valor * (this.creditoParcelado14 / 100)) - (valor * (this.parcela12 / 100))).toFixed(2));
		  

    		}else if(parcelamento == "Cliente"){
    			this.textoTaxas = "Você irá receber no:";
				this.valorDebito = parseFloat("" + (valor - (valor * (this.debito / 100))).toFixed(2)); 
				this.valorParcela1 = parseFloat("" + (valor - (valor * (this.credito14 / 100))).toFixed(2));
		    	this.valorParcela2 = parseFloat("" + (valor - (valor * (this.creditoParcelado14 / 100))).toFixed(2));
		    	this.valorParcela3 = parseFloat("" + (valor - (valor * (this.creditoParcelado14 / 100))).toFixed(2));
		    	this.valorParcela4 = parseFloat("" + (valor - (valor * (this.creditoParcelado14 / 100))).toFixed(2));
		    	this.valorParcela5 = parseFloat("" + (valor - (valor * (this.creditoParcelado14 / 100))).toFixed(2));
		    	this.valorParcela6 = parseFloat("" + (valor - (valor * (this.creditoParcelado14 / 100))).toFixed(2));
		    	this.valorParcela7 = parseFloat("" + (valor - (valor * (this.creditoParcelado14 / 100))).toFixed(2));
		    	this.valorParcela8 = parseFloat("" + (valor - (valor * (this.creditoParcelado14 / 100))).toFixed(2));
		    	this.valorParcela9 = parseFloat("" + (valor - (valor * (this.creditoParcelado14 / 100))).toFixed(2));
		    	this.valorParcela10 = parseFloat("" + (valor - (valor * (this.creditoParcelado14 / 100))).toFixed(2));
		    	this.valorParcela11 = parseFloat("" + (valor - (valor * (this.creditoParcelado14 / 100))).toFixed(2));
		    	this.valorParcela12 = parseFloat("" + (valor - (valor * (this.creditoParcelado14 / 100))).toFixed(2));
		 
    		}
    	} else if (recebimento == "30") {
    		if (parcelamento == "Vendedor") {
    			this.textoTaxas = "Você irá receber no:";
				this.valorDebito = parseFloat("" + (valor - (valor * (this.debito / 100))).toFixed(2));
				this.valorParcela1 = parseFloat("" + (valor - (valor * (this.credito30 / 100))).toFixed(2));
				this.valorParcela2 = parseFloat("" + (valor - (valor * (this.creditoParcelado30 / 100)) - (valor * (this.parcela2 / 100))).toFixed(2));
				this.valorParcela3 = parseFloat("" + (valor - (valor * (this.creditoParcelado30 / 100)) - (valor * (this.parcela3 / 100))).toFixed(2));
				this.valorParcela4 = parseFloat("" + (valor - (valor * (this.creditoParcelado30 / 100)) - (valor * (this.parcela4 / 100))).toFixed(2));
				this.valorParcela5 = parseFloat("" + (valor - (valor * (this.creditoParcelado30 / 100)) - (valor * (this.parcela5 / 100))).toFixed(2));
				this.valorParcela6 = parseFloat("" + (valor - (valor * (this.creditoParcelado30 / 100)) - (valor * (this.parcela6 / 100))).toFixed(2));
				this.valorParcela7 = parseFloat("" + (valor - (valor * (this.creditoParcelado30 / 100)) - (valor * (this.parcela7 / 100))).toFixed(2));
				this.valorParcela8 = parseFloat("" + (valor - (valor * (this.creditoParcelado30 / 100)) - (valor * (this.parcela8 / 100))).toFixed(2));
				this.valorParcela9 = parseFloat("" + (valor - (valor * (this.creditoParcelado30 / 100)) - (valor * (this.parcela9 / 100))).toFixed(2));
				this.valorParcela10 = parseFloat("" + (valor - (valor * (this.creditoParcelado30 / 100)) - (valor * (this.parcela10 / 100))).toFixed(2));
				this.valorParcela11 = parseFloat("" + (valor - (valor * (this.creditoParcelado30 / 100)) - (valor * (this.parcela11 / 100))).toFixed(2));
				this.valorParcela12 = parseFloat("" + (valor - (valor * (this.creditoParcelado30 / 100)) - (valor * (this.parcela12 / 100))).toFixed(2));
		 

    		}else if(parcelamento == "Cliente"){
    			this.textoTaxas = "Você irá receber no:";
				this.valorDebito = parseFloat("" + (valor - (valor * (this.debito / 100))).toFixed(2)); 
				this.valorParcela1 = parseFloat("" + (valor - (valor * (this.credito30 / 100))).toFixed(2));
		    	this.valorParcela2 = parseFloat("" + (valor - (valor * (this.creditoParcelado30 / 100))).toFixed(2));
		    	this.valorParcela3 = parseFloat("" + (valor - (valor * (this.creditoParcelado30 / 100))).toFixed(2));
		    	this.valorParcela4 = parseFloat("" + (valor - (valor * (this.creditoParcelado30 / 100))).toFixed(2));
		    	this.valorParcela5 = parseFloat("" + (valor - (valor * (this.creditoParcelado30 / 100))).toFixed(2));
		    	this.valorParcela6 = parseFloat("" + (valor - (valor * (this.creditoParcelado30 / 100))).toFixed(2));
		    	this.valorParcela7 = parseFloat("" + (valor - (valor * (this.creditoParcelado30 / 100))).toFixed(2));
		    	this.valorParcela8 = parseFloat("" + (valor - (valor * (this.creditoParcelado30 / 100))).toFixed(2));
		    	this.valorParcela9 = parseFloat("" + (valor - (valor * (this.creditoParcelado30 / 100))).toFixed(2));
		    	this.valorParcela10 = parseFloat("" + (valor - (valor * (this.creditoParcelado30 / 100))).toFixed(2));
		    	this.valorParcela11 = parseFloat("" + (valor - (valor * (this.creditoParcelado30 / 100))).toFixed(2));
		    	this.valorParcela12 = parseFloat("" + (valor - (valor * (this.creditoParcelado30 / 100))).toFixed(2));
		 
    		}
    	}      	
    } else if (tipo == "Reverso") {
    	if (recebimento == "Hora") {
    		if (parcelamento == "Vendedor") {
    			this.textoTaxas = "Você deverá cobrar no:";
				this.valorDebito = parseFloat("" + (valor + (valor * ((this.debito / 100) + 0.0006))).toFixed(2));
				this.valorParcela1 = parseFloat("" + (valor + (valor * ((this.creditoHora / 100) + 0.0023))).toFixed(2));
				this.valorParcela2 = parseFloat("" + (valor + (valor * ((this.creditoParceladoHora / 100) + 0.0055)) + (valor * ((this.parcela2 / 100) + 0.0043))).toFixed(2));
				this.valorParcela3 = parseFloat("" + (valor + (valor * ((this.creditoParceladoHora / 100) + 0.0064)) + (valor * ((this.parcela3 / 100) + 0.0065))).toFixed(2));
				this.valorParcela4 = parseFloat("" + (valor + (valor * ((this.creditoParceladoHora / 100) + 0.0073)) + (valor * ((this.parcela4 / 100) + 0.0092))).toFixed(2));
				this.valorParcela5 = parseFloat("" + (valor + (valor * ((this.creditoParceladoHora / 100) + 0.0081)) + (valor * ((this.parcela5 / 100) + 0.0122))).toFixed(2));
				this.valorParcela6 = parseFloat("" + (valor + (valor * ((this.creditoParceladoHora / 100) + 0.009)) + (valor * ((this.parcela6 / 100) + 0.0156))).toFixed(2));
				this.valorParcela7 = parseFloat("" + (valor + (valor * ((this.creditoParceladoHora / 100) + 0.0099)) + (valor * ((this.parcela7 / 100) + 0.0194))).toFixed(2));
				this.valorParcela8 = parseFloat("" + (valor + (valor * ((this.creditoParceladoHora / 100) + 0.0108)) + (valor * ((this.parcela8 / 100) + 0.0237))).toFixed(2));
				this.valorParcela9 = parseFloat("" + (valor + (valor * ((this.creditoParceladoHora / 100) + 0.0118)) + (valor * ((this.parcela9 / 100) + 0.0282))).toFixed(2));
				this.valorParcela10 = parseFloat("" + (valor + (valor * ((this.creditoParceladoHora / 100) + 0.0127)) + (valor * ((this.parcela10 / 100) + 0.0332))).toFixed(2));
				this.valorParcela11 = parseFloat("" + (valor + (valor * ((this.creditoParceladoHora / 100) + 0.0136)) + (valor * ((this.parcela11 / 100) + 0.0385))).toFixed(2));
				this.valorParcela12 = parseFloat("" + (valor + (valor * ((this.creditoParceladoHora / 100) + 0.0145)) + (valor * ((this.parcela12 / 100) + 0.0442))).toFixed(2));
		    	

    		}else if(parcelamento == "Cliente"){
    			this.textoTaxas = "Você deverá cobrar no:";
				this.valorDebito = parseFloat("" + (valor + (valor * ((this.debito / 100) + 0.0006))).toFixed(2)); 
				this.valorParcela1 = parseFloat("" + (valor + (valor * ((this.creditoHora / 100) + 0.0024))).toFixed(2));
		    	this.valorParcela2 = parseFloat("" + (valor + (valor * ((this.creditoParceladoHora / 100) + 0.003))).toFixed(2));
		    	this.valorParcela3 = parseFloat("" + (valor + (valor * ((this.creditoParceladoHora / 100) + 0.003))).toFixed(2));
		    	this.valorParcela4 = parseFloat("" + (valor + (valor * ((this.creditoParceladoHora / 100) + 0.003))).toFixed(2));
		    	this.valorParcela5 = parseFloat("" + (valor + (valor * ((this.creditoParceladoHora / 100) + 0.003))).toFixed(2));
		    	this.valorParcela6 = parseFloat("" + (valor + (valor * ((this.creditoParceladoHora / 100) + 0.003))).toFixed(2));
		    	this.valorParcela7 = parseFloat("" + (valor + (valor * ((this.creditoParceladoHora / 100) + 0.003))).toFixed(2));
		    	this.valorParcela8 = parseFloat("" + (valor + (valor * ((this.creditoParceladoHora / 100) + 0.003))).toFixed(2));
		    	this.valorParcela9 = parseFloat("" + (valor + (valor * ((this.creditoParceladoHora / 100) + 0.003))).toFixed(2));
		    	this.valorParcela10 = parseFloat("" + (valor + (valor * ((this.creditoParceladoHora / 100) + 0.003))).toFixed(2));
		    	this.valorParcela11 = parseFloat("" + (valor + (valor * ((this.creditoParceladoHora / 100) + 0.003))).toFixed(2));
		    	this.valorParcela12 = parseFloat("" + (valor + (valor * ((this.creditoParceladoHora / 100) + 0.003))).toFixed(2));
    			
    		}
    	} else if (recebimento == "14") {
    		if (parcelamento == "Vendedor") {
    			this.textoTaxas = "Você deverá cobrar no:";
				this.valorDebito = parseFloat("" + (valor + (valor * ((this.debito / 100) + 0.0006))).toFixed(2));
				this.valorParcela1 = parseFloat("" + (valor + (valor * ((this.credito14 / 100) + 0.0015))).toFixed(2));
				this.valorParcela2 = parseFloat("" + (valor + (valor * ((this.creditoParcelado14 / 100) + 0.004)) + (valor * ((this.parcela2 / 100) + 0.0038))).toFixed(2));
				this.valorParcela3 = parseFloat("" + (valor + (valor * ((this.creditoParcelado14 / 100) + 0.0047)) + (valor * ((this.parcela3 / 100) + 0.0059))).toFixed(2));
				this.valorParcela4 = parseFloat("" + (valor + (valor * ((this.creditoParcelado14 / 100) + 0.0054)) + (valor * ((this.parcela4 / 100) + 0.0084))).toFixed(2));
				this.valorParcela5 = parseFloat("" + (valor + (valor * ((this.creditoParcelado14 / 100) + 0.0062)) + (valor * ((this.parcela5 / 100) + 0.0112))).toFixed(2));
				this.valorParcela6 = parseFloat("" + (valor + (valor * ((this.creditoParcelado14 / 100) + 0.0069)) + (valor * ((this.parcela6 / 100) + 0.0144))).toFixed(2));
				this.valorParcela7 = parseFloat("" + (valor + (valor * ((this.creditoParcelado14 / 100) + 0.0076)) + (valor * ((this.parcela7 / 100) + 0.018))).toFixed(2));
				this.valorParcela8 = parseFloat("" + (valor + (valor * ((this.creditoParcelado14 / 100) + 0.0083)) + (valor * ((this.parcela8 / 100) + 0.0221))).toFixed(2));
				this.valorParcela9 = parseFloat("" + (valor + (valor * ((this.creditoParcelado14 / 100) + 0.009)) + (valor * ((this.parcela9 / 100) + 0.0264))).toFixed(2));
				this.valorParcela10 = parseFloat("" + (valor + (valor * ((this.creditoParcelado14 / 100) + 0.0098)) + (valor * ((this.parcela10 / 100) + 0.0311))).toFixed(2));
				this.valorParcela11 = parseFloat("" + (valor + (valor * ((this.creditoParcelado14 / 100) + 0.0105)) + (valor * ((this.parcela11 / 100) + 0.0362))).toFixed(2));
				this.valorParcela12 = parseFloat("" + (valor + (valor * ((this.creditoParcelado14 / 100) + 0.0113)) + (valor * ((this.parcela12 / 100) + 0.0417))).toFixed(2));

    		}else if(parcelamento == "Cliente"){
    			this.textoTaxas = "Você deverá cobrar no:";
				this.valorDebito = parseFloat("" + (valor + (valor * ((this.debito / 100) + 0.0006))).toFixed(2)); 
				this.valorParcela1 = parseFloat("" + (valor + (valor * ((this.credito14 / 100) + 0.0015))).toFixed(2));
		    	this.valorParcela2 = parseFloat("" + (valor + (valor * ((this.creditoParcelado14 / 100) + 0.002))).toFixed(2));
		    	this.valorParcela3 = parseFloat("" + (valor + (valor * ((this.creditoParcelado14 / 100) + 0.002))).toFixed(2));
		    	this.valorParcela4 = parseFloat("" + (valor + (valor * ((this.creditoParcelado14 / 100) + 0.002))).toFixed(2));
		    	this.valorParcela5 = parseFloat("" + (valor + (valor * ((this.creditoParcelado14 / 100) + 0.002))).toFixed(2));
		    	this.valorParcela6 = parseFloat("" + (valor + (valor * ((this.creditoParcelado14 / 100) + 0.002))).toFixed(2));
		    	this.valorParcela7 = parseFloat("" + (valor + (valor * ((this.creditoParcelado14 / 100) + 0.002))).toFixed(2));
		    	this.valorParcela8 = parseFloat("" + (valor + (valor * ((this.creditoParcelado14 / 100) + 0.002))).toFixed(2));
		    	this.valorParcela9 = parseFloat("" + (valor + (valor * ((this.creditoParcelado14 / 100) + 0.002))).toFixed(2));
		    	this.valorParcela10 = parseFloat("" + (valor + (valor * ((this.creditoParcelado14 / 100) + 0.002))).toFixed(2));
		    	this.valorParcela11 = parseFloat("" + (valor + (valor * ((this.creditoParcelado14 / 100) + 0.002))).toFixed(2));
		    	this.valorParcela12 = parseFloat("" + (valor + (valor * ((this.creditoParcelado14 / 100) + 0.002))).toFixed(2));
    		}
    	} else if (recebimento == "30") {
    		if (parcelamento == "Vendedor") {
    			this.textoTaxas = "Você deverá cobrar no:";
				this.valorDebito = parseFloat("" + (valor + (valor * ((this.debito / 100) + 0.0006))).toFixed(2));
				this.valorParcela1 = parseFloat("" + (valor + (valor * ((this.credito30 / 100) + 0.001))).toFixed(2));
				this.valorParcela2 = parseFloat("" + (valor + (valor * ((this.creditoParcelado30 / 100) + 0.003)) + (valor * ((this.parcela2 / 100) + 0.0034))).toFixed(2));
				this.valorParcela3 = parseFloat("" + (valor + (valor * ((this.creditoParcelado30 / 100) + 0.0036)) + (valor * ((this.parcela3 / 100) + 0.0054))).toFixed(2));
				this.valorParcela4 = parseFloat("" + (valor + (valor * ((this.creditoParcelado30 / 100) + 0.0042)) + (valor * ((this.parcela4 / 100) + 0.0206))).toFixed(2));
				this.valorParcela5 = parseFloat("" + (valor + (valor * ((this.creditoParcelado30 / 100) + 0.0047)) + (valor * ((this.parcela5 / 100) + 0.0104))).toFixed(2));
				this.valorParcela6 = parseFloat("" + (valor + (valor * ((this.creditoParcelado30 / 100) + 0.0053)) + (valor * ((this.parcela6 / 100) + 0.0135))).toFixed(2));
				this.valorParcela7 = parseFloat("" + (valor + (valor * ((this.creditoParcelado30 / 100) + 0.0059)) + (valor * ((this.parcela7 / 100) + 0.017))).toFixed(2));
				this.valorParcela8 = parseFloat("" + (valor + (valor * ((this.creditoParcelado30 / 100) + 0.0065)) + (valor * ((this.parcela8 / 100) + 0.0209))).toFixed(2));
				this.valorParcela9 = parseFloat("" + (valor + (valor * ((this.creditoParcelado30 / 100) + 0.0071)) + (valor * ((this.parcela9 / 100) + 0.025))).toFixed(2));
				this.valorParcela10 = parseFloat("" + (valor + (valor * ((this.creditoParcelado30 / 100) + 0.0077)) + (valor * ((this.parcela10 / 100) + 0.0296))).toFixed(2));
				this.valorParcela11 = parseFloat("" + (valor + (valor * ((this.creditoParcelado30 / 100) + 0.0083)) + (valor * ((this.parcela11 / 100) + 0.0345))).toFixed(2));
				this.valorParcela12 = parseFloat("" + (valor + (valor * ((this.creditoParcelado30 / 100) + 0.0089)) + (valor * ((this.parcela12 / 100) + 0.0397))).toFixed(2));


    		}else if(parcelamento == "Cliente"){
    			this.textoTaxas = "Você deverá cobrar no:";
				this.valorDebito = parseFloat("" + (valor + (valor * ((this.debito / 100) + 0.0006))).toFixed(2)); 
				this.valorParcela1 = parseFloat("" + (valor + (valor * ((this.credito30 / 100) + 0.001))).toFixed(2));
		    	this.valorParcela2 = parseFloat("" + (valor + (valor * ((this.creditoParcelado30 / 100) + 0.0014))).toFixed(2));
		    	this.valorParcela3 = parseFloat("" + (valor + (valor * ((this.creditoParcelado30 / 100) + 0.0014))).toFixed(2));
		    	this.valorParcela4 = parseFloat("" + (valor + (valor * ((this.creditoParcelado30 / 100) + 0.0014))).toFixed(2));
		    	this.valorParcela5 = parseFloat("" + (valor + (valor * ((this.creditoParcelado30 / 100) + 0.0014))).toFixed(2));
		    	this.valorParcela6 = parseFloat("" + (valor + (valor * ((this.creditoParcelado30 / 100) + 0.0014))).toFixed(2));
		    	this.valorParcela7 = parseFloat("" + (valor + (valor * ((this.creditoParcelado30 / 100) + 0.0014))).toFixed(2));
		    	this.valorParcela8 = parseFloat("" + (valor + (valor * ((this.creditoParcelado30 / 100) + 0.0014))).toFixed(2));
		    	this.valorParcela9 = parseFloat("" + (valor + (valor * ((this.creditoParcelado30 / 100) + 0.0014))).toFixed(2));
		    	this.valorParcela10 = parseFloat("" + (valor + (valor * ((this.creditoParcelado30 / 100) + 0.0014))).toFixed(2));
		    	this.valorParcela11 = parseFloat("" + (valor + (valor * ((this.creditoParcelado30 / 100) + 0.0014))).toFixed(2));
		    	this.valorParcela12 = parseFloat("" + (valor + (valor * ((this.creditoParcelado30 / 100) + 0.0014))).toFixed(2));
    		}
    	} 
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResultadoCartaoPage');
  }



}
