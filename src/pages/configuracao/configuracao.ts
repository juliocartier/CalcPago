import { MercadoPagoPage } from './../mercado-pago/mercado-pago';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { AdMobFree, AdMobFreeInterstitialConfig, AdMobFreeBannerConfig } from '@ionic-native/admob-free';

/**
 * Generated class for the ConfiguracaoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-configuracao',
  templateUrl: 'configuracao.html',
})
export class ConfiguracaoPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public translate: TranslateService, private admobFree: AdMobFree) {
   // this.showInterstialAd();
   // this.showBannerAd();
   // this.showVideoAd();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfiguracaoPage');
  }

  /** 
   * Corrigir a linguagem a partir da configuração
  */

  valorConfiguracao(debito: any, creditoHora: any, credito14: any, credito30: any, creditoParceladoHora: any, creditoParcelado14 : any, 
    creditoParcelado30: any, parcela2: any, parcela3: any, parcela4: any, parcela5: any, parcela6: any, parcela7: any, parcela8: any, parcela9: any,
    parcela10: any, parcela11: any, parcela12: any){

    let dados = {
      debito: debito,
      creditoHora: creditoHora,
      credito14: credito14,
      credito30: credito30,
      creditoParceladoHora: creditoParceladoHora,
      creditoParcelado14: creditoParcelado14,
      creditoParcelado30: creditoParcelado30,
      parcela2: parcela2,
      parcela3: parcela3,
      parcela4: parcela4,
      parcela5: parcela5,
      parcela6: parcela6,
      parcela7: parcela7,
      parcela8: parcela8,
      parcela9: parcela9,
      parcela10: parcela10,
      parcela11: parcela11,
      parcela12: parcela12
      //tipoMoeda : tipoMoeda
      //moeda: moeda
    };

    //console.log(myData);
    this.navCtrl.setRoot(MercadoPagoPage, dados);
    //console.log(dados);

   // console.log(debito, creditoHora, credito14, credito30, creditoParceladoHora, creditoParcelado14, creditoParcelado30, parcela2,
   // parcela3, parcela4, parcela5, parcela6, parcela7, parcela8, parcela9, parcela10, parcela11, parcela12);
  }

  async showInterstialAd() {
    try {
      const interstitialConfig: AdMobFreeInterstitialConfig = {
        id: 'ca-app-pub-4250164210737037/5911486633',
        isTesting: false,
        autoShow: true
      }
      this.admobFree.interstitial.config(interstitialConfig);
      const result = await this.admobFree.interstitial.prepare();
    } catch (e) {
      // statements
      console.log(e);
    }
  }

  async showBannerAd() {
    const bannerConfig: AdMobFreeBannerConfig = {
      id: 'ca-app-pub-4250164210737037/4131123805',
      isTesting: false,
      autoShow: true
    }

    this.admobFree.banner.config(bannerConfig);

    try {
      const result = this.admobFree.banner.prepare();
    } catch (e) {
      console.error(e);
    }

  }

  async showVideoAd() {
    try {
      const interstitialConfig: AdMobFreeInterstitialConfig = {
        id: 'ca-app-pub-4250164210737037/5911486633',
        isTesting: false,
        autoShow: true
      }

      this.admobFree.interstitial.config(interstitialConfig);

      const result = await this.admobFree.interstitial.prepare();

    } catch (e) {
      // statements
      console.log(e);
    }

  }

}
