import { Component } from '@angular/core';
import { IonicPage, NavParams, NavController, ViewController } from 'ionic-angular';
import { Screenshot } from '@ionic-native/screenshot';
import { AdMobFree, AdMobFreeInterstitialConfig, AdMobFreeBannerConfig} from '@ionic-native/admob-free';
import { ResultadoCartaoPage } from '../resultado-cartao/resultado-cartao';
import { TranslateService } from '@ngx-translate/core';

/**
 * Generated class for the ModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal',
  templateUrl: 'modal.html',
})
export class ModalPage {

	tipoModo: string = this.navParams.get('calculoTaxas');
	valorTotal : any = this.navParams.get('valor');
	tipoMaquina : string = this.navParams.get('maquina');
	tipoPagamento : string = this.navParams.get('pagamento');
	qtdParcelas : any = this.navParams.get('parcelas');
	tipoRecebimento : string = this.navParams.get('recebimento');
  tipoParcelamento : string = this.navParams.get('parcelamento');
  lang: string = this.navParams.get('lang');

  /**
   * Recebendo a alteração de valores do modulo mercado pago
   * Debito, Credito Hora...
   */
  debito: any = this.navParams.get('debito');
  creditoHora: any = this.navParams.get('creditoHora');
  credito14: any = this.navParams.get('credito14');
  credito30: any = this.navParams.get('credito30');
  creditoParceladoHora: any = this.navParams.get('creditoParceladoHora');
  creditoParcelado14: any = this.navParams.get('creditoParcelado14');
  creditoParcelado30: any = this.navParams.get('creditoParcelado30');
  parcela2: any = this.navParams.get('parcela2');
  parcela3: any = this.navParams.get('parcela3');
  parcela4: any = this.navParams.get('parcela4');
  parcela5: any = this.navParams.get('parcela5');
  parcela6: any = this.navParams.get('parcela6');
  parcela7: any = this.navParams.get('parcela7');
  parcela8: any = this.navParams.get('parcela8');
  parcela9: any = this.navParams.get('parcela9');
  parcela10: any = this.navParams.get('parcela10');
  parcela11: any = this.navParams.get('parcela11');
  parcela12: any = this.navParams.get('parcela12');

	taxaCliente : any;
	valorAux : any;
	valorParcela : any;
	totalImp : any;
	totalParcelamento : any;
	totalTaxas : any;
	total : any;
	valor : any;
	totalCliente : any;
	valorRecebido : any;
	taxasAux : any;
	taxaParcelamentoAux : any;
  porcentagemVendedor : any;
  porcentagemMaquina : any;
  porcentagemCliente : any;
  totalPorVendedor : any;
  totalPorCliente : any;
  totalPorcentagemV : any;
  screen: any;
  state: boolean = false;



  constructor(public navCtrl: NavController, private navParams: NavParams, private view: ViewController, private screenshot: Screenshot, private admobFree: AdMobFree, public translate: TranslateService) {
  	  this.showVideoAd();
      this.showBannerAd();
      this.showInterstialAd();
      this.funcaoPointPago();
  }

  switchLanguage() {
    this.translate.use(this.lang);
  }

    async showInterstialAd(){
    try {
      const interstitialConfig: AdMobFreeInterstitialConfig = {
        id: 'ca-app-pub-4250164210737037/5911486633',
        isTesting: false,
        autoShow: true
      }

      this.admobFree.interstitial.config(interstitialConfig);

      const result = await this.admobFree.interstitial.prepare();
      //console.log("Esta funcionando", result);

    } catch(e) {
      // statements
      console.log(e);
    }
  }

  async showBannerAd(){
    const bannerConfig: AdMobFreeBannerConfig = {
      id: 'ca-app-pub-4250164210737037/4131123805',
      isTesting: false,
      autoShow: true
      //bannerAtTop: true
    }

    this.admobFree.banner.config(bannerConfig);

    try {
      const result = this.admobFree.banner.prepare();
    } catch(e) {
      // statements
      //console.log(e);
      console.error(e);
    }
    
  }

    async showVideoAd(){
      try {
        const interstitialConfig: AdMobFreeInterstitialConfig = {
        id: 'ca-app-pub-4250164210737037/5911486633',
        isTesting: false,
        autoShow: true
      }

      this.admobFree.interstitial.config(interstitialConfig);

      const result = await this.admobFree.interstitial.prepare();
      //console.log("Esta funcionando", result);

    } catch(e) {
      // statements
      console.log(e);
    }
    
  }

     ResultadoCartao() {
      let tipo = this.tipoModo;
      let valor = parseFloat(this.valorTotal);
      let recebimento = this.tipoRecebimento;
      let parcelamento = this.tipoParcelamento;
      let lang = this.lang;
      let debito = this.debito;
      let creditoHora = this.creditoHora;
      let credito14 = this.credito14;
      let credito30 = this.credito30;
      let creditoParceladoHora = this.creditoParceladoHora;
      let creditoParcelado14 = this.creditoParcelado14;
      let creditoParcelado30 = this.creditoParcelado30;
      let parcela2 = this.parcela2;
      let parcela3 = this.parcela3;
      let parcela4 = this.parcela4;
      let parcela5 = this.parcela5;
      let parcela6 = this.parcela6;
      let parcela7 = this.parcela7;
      let parcela8 = this.parcela8;
      let parcela9 = this.parcela9;
      let parcela10 = this.parcela10;
      let parcela11 = this.parcela11;
      let parcela12 = this.parcela12;

      let myValor = {
      calculoTaxas: tipo,
      valor: valor,
      recebimento : recebimento,
      parcelamento : parcelamento,
      lang: lang,
      debito: debito,
      creditoHora: creditoHora,
      credito14: credito14,
      credito30: credito30,
      creditoParceladoHora: creditoParceladoHora,
      creditoParcelado14: creditoParcelado14,
      creditoParcelado30: creditoParcelado30,
      parcela2: parcela2,
      parcela3: parcela3,
      parcela4: parcela4,
      parcela5: parcela5,
      parcela6: parcela6,
      parcela7: parcela7,
      parcela8: parcela8,
      parcela9: parcela9,
      parcela10: parcela10,
      parcela11: parcela11,
      parcela12: parcela12
    };
    console.log(myValor);
    this.navCtrl.push(ResultadoCartaoPage, myValor); 
   }

  funcaoPointPago(){

      let tipo = this.tipoModo;
      let valor = this.valorTotal;
      let maquina = this.tipoMaquina;
      let pagamento = this.tipoPagamento;
      let parcelas = this.qtdParcelas;
      let recebimento = this.tipoRecebimento;
      let parcelamento = this.tipoParcelamento;
      //let moeda = this.moeda;
      //console.log(this.moeda);  

      //Calculo das taxas para o mercado pago
      if (tipo == "Taxas") {
        if (maquina == "Point") {//Inicio do if da maquina Point
          if (recebimento == "Hora") {// Inicio do if do recebimento
            if (pagamento == "Debito") { //Inicio do if do tipo de pagamento
              if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                //Calculos
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = 1;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                this.totalPorCliente = this.taxaCliente;
                this.porcentagemVendedor = ((this.debito/100) * 100);
                this.totalPorcentagemV = 0;
                this.totalImp = valor * (this.debito/100);
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.totalPorVendedor = this.porcentagemVendedor;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;
              //Fim do if do Vendedor 
              } 
               //Fim do if do tipo de pagamento 
              } else if (pagamento == "Credito"){ // Inicio do if do tipo credito
                if(parcelas == 1){ //Inicio do if da quantidade de parcelas = 1 
                if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                //Calculos
                
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                this.porcentagemVendedor = ((this.creditoHora/100) * 100);
                this.totalPorVendedor = this.porcentagemVendedor;
                this.totalPorCliente = this.taxaCliente;
                this.totalPorcentagemV = 0;
                this.totalImp = valor * (this.creditoHora / 100);
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;

              //Fim do if do Vendedor 
              } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                //Calculos
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                this.porcentagemVendedor = ((this.creditoHora / 100) * 100);
                this.totalPorVendedor = this.porcentagemVendedor;
                this.totalPorCliente = this.taxaCliente;
                this.totalPorcentagemV = 0;
                //console.log(this.valorParcela);
                this.totalImp = valor * (this.creditoHora / 100);
                this.totalParcelamento = valor * this.taxaCliente;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;
              } // fim do if do cliente

              }// fim do if da quantidade de parcelas = 1

              else if(parcelas == 2){ // Inicio do if da quantidade de parcelas = 2
                if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                //Calculos
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                //console.log(this.valorParcela);
                this.porcentagemVendedor = ((this.creditoParceladoHora/100) * 100); // valor da tarifa em hora
                this.totalPorcentagemV = ((this.parcela2/100) * 100); // valor da parcela 2 = 4.09
                this.totalPorCliente = 0;
                this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                this.totalImp = valor * (this.creditoParceladoHora / 100);
                this.totalParcelamento = valor * (this.parcela2 / 100);
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;
              //Fim do if do Vendedor 
              } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                //Calculos
                //this.parcelamentoAux = 0.0409;
                this.taxaCliente = valor * (this.parcela2 / 100); //Valor da parcela 2
                this.valorAux = valor;
                this.valorRecebido = valor + this.taxaCliente;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                //console.log(this.valorParcela);
                this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100); //Valor da taxa parcelado por hora
                this.totalPorcentagemV = 0;
                this.totalPorCliente = ((this.parcela2 / 100) * 100);
                this.totalPorVendedor = this.porcentagemVendedor;
                this.totalImp = valor * (this.creditoParceladoHora / 100);
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = this.valorRecebido;
              } // fim do if do cliente
              
              }// Fim do if da quantidade de parcelas = 2
              else if(parcelas == 3){ // Inicio do if da quantidade de parcelas = 3
                if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                //Calculos
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                //console.log(this.valorParcela);
                  this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                  this.totalPorcentagemV = ((this.parcela3 / 100) * 100);
                this.totalPorCliente = 0;
                this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                  this.totalImp = valor * (this.creditoParceladoHora / 100);
                  this.totalParcelamento = valor * (this.parcela3 / 100);
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor
              //Fim do if do Vendedor 
              } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                //Calculos
                //this.parcelamentoAux = 0.0409;
                this.taxaCliente = valor * (this.parcela3 / 100);
                this.valorAux = valor;
                this.valorRecebido = valor + this.taxaCliente;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                //console.log(this.valorParcela);
                this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                this.totalPorcentagemV = 0;
                this.totalPorCliente = (((this.parcela3 / 100) * 100)).toFixed(2);
                this.totalPorVendedor = this.porcentagemVendedor;
                this.totalImp = valor * (this.creditoParceladoHora / 100);
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = this.valorRecebido;
              } // fim do if do cliente
              
              }// Fim do if da quantidade de parcelas = 3
              else if(parcelas == 4){ // Inicio do if da quantidade de parcelas = 4
                if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                //Calculos
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                //console.log(this.valorParcela);
                this.totalImp = valor * (this.creditoParceladoHora / 100);
                this.totalParcelamento = valor * (this.parcela4 / 100);
                this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                this.totalPorcentagemV = ((this.parcela4 / 100) * 100);
                this.totalPorCliente = 0;
                this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;
              //Fim do if do Vendedor 
              } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                //Calculos
                //this.parcelamentoAux = 0.0409;
                this.taxaCliente = valor * (this.parcela4 / 100);
                this.valorAux = valor;
                this.valorRecebido = valor + this.taxaCliente;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                this.totalPorcentagemV = 0;
                this.totalPorCliente = (((this.parcela4 / 100) * 100)).toFixed(2);
                this.totalPorVendedor = this.porcentagemVendedor;
                //console.log(this.valorParcela);
                this.totalImp = valor * (this.creditoParceladoHora / 100);
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = this.valorRecebido;
              } // fim do if do cliente
              
              }// Fim do if da quantidade de parcelas = 4

              else if(parcelas == 5){ // Inicio do if da quantidade de parcelas = 5
                if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                //Calculos
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                //console.log(this.valorParcela);
                this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                this.totalPorcentagemV = (((this.parcela5 / 100) * 100));
                this.totalPorCliente = 0;
                this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                this.totalImp = valor * (this.creditoParceladoHora / 100);
                this.totalParcelamento = valor * (this.parcela5 / 100);
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;
              //Fim do if do Vendedor 
              } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                //Calculos
                //this.parcelamentoAux = 0.0409;
                  this.taxaCliente = valor * (this.parcela5 / 100);
                this.valorAux = valor;
                this.valorRecebido = valor + this.taxaCliente;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                //console.log(this.valorParcela);
                this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                this.totalPorcentagemV = 0;
                this.totalPorCliente = (((this.parcela5 / 100) * 100)).toFixed(2);
                this.totalPorVendedor = this.porcentagemVendedor;
                this.totalImp = valor * (this.creditoParceladoHora / 100);
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = this.valorRecebido;
              } // fim do if do cliente
              
              }// Fim do if da quantidade de parcelas = 5

              else if(parcelas == 6){ // Inicio do if da quantidade de parcelas = 6
                if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                //Calculos
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                //console.log(this.valorParcela);
                this.porcentagemVendedor = ((this.creditoParceladoHora / 100)* 100);
                  this.totalPorcentagemV = ((this.parcela6 / 100) * 100);
                this.totalPorCliente = 0;
                this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                this.totalImp = valor * (this.creditoParceladoHora / 100);
                  this.totalParcelamento = valor * (this.parcela6 / 100);
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;
              //Fim do if do Vendedor 
              } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                //Calculos
                //this.parcelamentoAux = 0.0409;
                this.taxaCliente = valor * (this.parcela6 / 100);
                this.valorAux = valor;
                this.valorRecebido = valor + this.taxaCliente;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                this.totalPorcentagemV = 0;
                this.totalPorCliente = (((this.parcela6 / 100) * 100)).toFixed(2);
                this.totalPorVendedor = this.porcentagemVendedor;
                //console.log(this.valorParcela);
                this.totalImp = valor * (this.creditoParceladoHora / 100);
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = this.valorRecebido;
              } // fim do if do cliente
              
              }// Fim do if da quantidade de parcelas = 6

              else if(parcelas == 7){ // Inicio do if da quantidade de parcelas = 7
                if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                //Calculos
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                this.totalPorcentagemV = ((this.parcela7 / 100) * 100);
                this.totalPorCliente = 0;
                this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                //console.log(this.valorParcela);
                this.totalImp = valor * (this.creditoParceladoHora / 100);
                this.totalParcelamento = valor * (this.parcela7 / 100);
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;
              //Fim do if do Vendedor 
              } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                //Calculos
                //this.parcelamentoAux = 0.0409;
                  this.taxaCliente = valor * (this.parcela7 / 100);
                this.valorAux = valor;
                this.valorRecebido = valor + this.taxaCliente;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                //console.log(this.valorParcela);
                this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                this.totalPorcentagemV = 0;
                  this.totalPorCliente = (((this.parcela7 / 100) * 100)).toFixed(2);
                this.totalPorVendedor = this.porcentagemVendedor;
                this.totalImp = valor * (this.creditoParceladoHora / 100);
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = this.valorRecebido;
              } // fim do if do cliente
              
              }// Fim do if da quantidade de parcelas = 7
              else if(parcelas == 8){ // Inicio do if da quantidade de parcelas = 8
                if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                //Calculos
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                //console.log(this.valorParcela);
                this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                  this.totalPorcentagemV = ((this.parcela8 / 100) * 100);
                this.totalPorCliente = 0;
                this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                this.totalImp = valor * (this.creditoParceladoHora / 100);
                  this.totalParcelamento = valor * (this.parcela8 / 100);
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;
              //Fim do if do Vendedor 
              } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                //Calculos
                //this.parcelamentoAux = 0.0409;
                  this.taxaCliente = valor * (this.parcela8 / 100);
                this.valorAux = valor;
                this.valorRecebido = valor + this.taxaCliente;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                //console.log(this.valorParcela);
                  this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                this.totalPorcentagemV = 0;
                  this.totalPorCliente = (((this.parcela8 / 100) * 100)).toFixed(2);
                this.totalPorVendedor = this.porcentagemVendedor;
                  this.totalImp = valor * (this.creditoParceladoHora / 100);
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = this.valorRecebido;
              } // fim do if do cliente
              
              }// Fim do if da quantidade de parcelas = 8
              else if(parcelas == 9){ // Inicio do if da quantidade de parcelas = 9
                if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                //Calculos
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                //console.log(this.valorParcela);
                this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                  this.totalPorcentagemV = ((this.parcela9 / 100) * 100);
                this.totalPorCliente = 0;
                this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                this.totalImp = valor * (this.creditoParceladoHora / 100);
                  this.totalParcelamento = valor * (this.parcela9 / 100);
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;
              //Fim do if do Vendedor 
              } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                //Calculos
                //this.parcelamentoAux = 0.0409;
                  this.taxaCliente = valor * (this.parcela9 / 100);
                this.valorAux = valor;
                this.valorRecebido = valor + this.taxaCliente;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                //console.log(this.valorParcela);
                  this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                this.totalPorcentagemV = 0;
                  this.totalPorCliente = (((this.parcela9 / 100) * 100)).toFixed(2);
                this.totalPorVendedor = this.porcentagemVendedor;
                  this.totalImp = valor * (this.creditoParceladoHora / 100);
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = this.valorRecebido;
              } // fim do if do cliente
              
              }// Fim do if da quantidade de parcelas = 9

              else if(parcelas == 10){ // Inicio do if da quantidade de parcelas = 10
                if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                //Calculos
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                //console.log(this.valorParcela);
                this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                  this.totalPorcentagemV = (this.parcela10 / 100) * 100;
                this.totalPorCliente = 0;
                this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                this.totalImp = valor * (this.creditoParceladoHora / 100);
                  this.totalParcelamento = valor * (this.parcela10 / 100);
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;
              //Fim do if do Vendedor 
              } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                //Calculos
                //this.parcelamentoAux = 0.0409;
                  this.taxaCliente = valor * (this.parcela10 / 100);
                this.valorAux = valor;
                this.valorRecebido = valor + this.taxaCliente;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                //console.log(this.valorParcela);
                  this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                this.totalPorcentagemV = 0;
                this.totalPorCliente = (((this.parcela10 / 100) * 100)).toFixed(2);
                this.totalPorVendedor = this.porcentagemVendedor;
                this.totalImp = valor * (this.creditoParceladoHora / 100);
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = this.valorRecebido;
              } // fim do if do cliente
              
              }// Fim do if da quantidade de parcelas = 10
              else if(parcelas == 11){ // Inicio do if da quantidade de parcelas = 11
                if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                //Calculos
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                //console.log(this.valorParcela);
                this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                this.totalPorcentagemV = (this.parcela11 / 100) * 100;
                this.totalPorCliente = 0;
                this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                this.totalImp = valor * (this.creditoParceladoHora / 100);
                this.totalParcelamento = valor * (this.parcela11 / 100);
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;
              //Fim do if do Vendedor 
              } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                //Calculos
                //this.parcelamentoAux = 0.0409;
                this.taxaCliente = valor * (this.parcela11 / 100);
                this.valorAux = valor;
                this.valorRecebido = valor + this.taxaCliente;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                //console.log(this.valorParcela);
                this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                this.totalPorcentagemV = 0;
                this.totalPorCliente = (((this.parcela11 / 100) * 100)).toFixed(2);
                this.totalPorVendedor = this.porcentagemVendedor;
                this.totalImp = valor * (this.creditoParceladoHora / 100);
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = this.valorRecebido;
              } // fim do if do cliente
              
              }// Fim do if da quantidade de parcelas = 11
              else if(parcelas == 12){ // Inicio do if da quantidade de parcelas = 12
                if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                //Calculos
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                //console.log(this.valorParcela);
                this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                this.totalPorcentagemV = (this.parcela12 / 100) * 100;
                this.totalPorCliente = 0;
                this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                this.totalImp = valor * (this.creditoParceladoHora / 100);
                this.totalParcelamento = valor * (this.parcela12 / 100);
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;
              //Fim do if do Vendedor 
              } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                //Calculos
                //this.parcelamentoAux = 0.0409;
                this.taxaCliente = valor * (this.parcela12 / 100);
                this.valorAux = valor;
                this.valorRecebido = valor + this.taxaCliente;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                //console.log(this.valorParcela);
                this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                this.totalPorcentagemV = 0;
                this.totalPorCliente = (((this.parcela12 / 100) * 100)).toFixed(2);
                this.totalPorVendedor = this.porcentagemVendedor;
                this.totalImp = valor * (this.creditoParceladoHora / 100);
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = this.valorRecebido;
              } // fim do if do cliente
              }// Fim do if da quantidade de parcelas = 12
            }// fim do if do tipo pagamento credito
          } //Fim do if do recebimento na hora
           else if (recebimento == "14"){ //Inicio do if para receber em 14 dias
              if (pagamento == "Credito") { // Inicio do if para receber em 14 dias credito
                if(parcelas == 1){ //Inicio do if da quantidade de parcelas 1
                  if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                //console.log(this.valorParcela);
                this.porcentagemVendedor = ((this.credito14/100) * 100);
                this.totalPorcentagemV = 0;
                this.totalPorCliente = 0;
                this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                this.totalImp = valor * (this.credito14/100);
                this.totalParcelamento = valor * this.taxaCliente;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;


              //Fim do if do Vendedor 
              } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                //Calculos
              this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                //console.log(this.valorParcela);
                this.porcentagemVendedor = ((this.credito14 / 100) * 100);
                this.totalPorcentagemV = 0;
                this.totalPorCliente = 0;
                this.totalPorVendedor = this.porcentagemVendedor;
                this.totalImp = valor * (this.credito14 / 100);
                this.totalParcelamento = valor * this.taxaCliente;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;
              } // fim do if do cliente

              }//Fim do if da quantidade de parcelas 1

              else if(parcelas == 2){ // Inicio do if da quantidade de parcelas = 2
                if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                //Calculos
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                //console.log(this.valorParcela);
                this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                this.totalPorcentagemV = ((this.parcela2 / 100) * 100);
                this.totalPorCliente = 0;
                this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                  this.totalImp = valor * (this.creditoParcelado14 / 100);
                this.totalParcelamento = valor * (this.parcela2 / 100);
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;
              //Fim do if do Vendedor 
              } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                //Calculos
                //this.parcelamentoAux = 0.0409;
                  this.taxaCliente = valor * (this.parcela2 / 100);
                this.valorAux = valor;
                this.valorRecebido = valor + this.taxaCliente;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                  this.porcentagemVendedor = ((this.creditoParcelado14 / 100)  * 100);
                this.totalPorcentagemV = 0;
                this.totalPorCliente = ((this.parcela2 / 100) * 100);
                this.totalPorVendedor = this.porcentagemVendedor;
                //console.log(this.valorParcela);
                  this.totalImp = valor * (this.creditoParcelado14 / 100) ;
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = this.valorRecebido;
              } // fim do if do cliente
              
              }// Fim do if da quantidade de parcelas = 2
              else if(parcelas == 3){ // Inicio do if da quantidade de parcelas = 3
                if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                //Calculos
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                //console.log(this.valorParcela);
                  this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                  this.totalPorcentagemV = ((this.parcela3 / 100) * 100);
                this.totalPorCliente = 0;
                this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                  this.totalImp = valor * (this.creditoParcelado14 / 100);
                  this.totalParcelamento = valor * (this.parcela3 / 100);
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;
              //Fim do if do Vendedor 
              } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                //Calculos
                //this.parcelamentoAux = 0.0409;
                  this.taxaCliente = valor * (this.parcela3 / 100);
                this.valorAux = valor;
                this.valorRecebido = valor + this.taxaCliente;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                //console.log(this.valorParcela);
                  this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                this.totalPorcentagemV = 0;
                  this.totalPorCliente = ((this.parcela3 / 100) * 100);
                this.totalPorVendedor = this.porcentagemVendedor;
                  this.totalImp = valor * (this.creditoParcelado14 / 100);
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = this.valorRecebido;
              } // fim do if do cliente
              
              }// Fim do if da quantidade de parcelas = 3
              else if(parcelas == 4){ // Inicio do if da quantidade de parcelas = 4
                if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                //Calculos
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                //console.log(this.valorParcela);
                  this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                  this.totalPorcentagemV = ((this.parcela4 / 100) * 100);
                this.totalPorCliente = 0;
                this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                  this.totalImp = valor * (this.creditoParcelado14 / 100);
                  this.totalParcelamento = valor * (this.parcela4 / 100);
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;
              //Fim do if do Vendedor 
              } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                //Calculos
                //this.parcelamentoAux = 0.0409;
                  this.taxaCliente = valor * (this.parcela4 / 100);
                this.valorAux = valor;
                this.valorRecebido = valor + this.taxaCliente;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                //console.log(this.valorParcela);
                  this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                this.totalPorcentagemV = 0;
                  this.totalPorCliente = ((this.parcela4 / 100) * 100);
                this.totalPorVendedor = this.porcentagemVendedor;
                  this.totalImp = valor * (this.creditoParcelado14 / 100);
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = this.valorRecebido;
              } // fim do if do cliente
              
              }// Fim do if da quantidade de parcelas = 4
                else if(parcelas == 5){ // Inicio do if da quantidade de parcelas = 5
                if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                //Calculos
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                //console.log(this.valorParcela);
                  this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                  this.totalPorcentagemV = ((this.parcela5 / 100) * 100);
                this.totalPorCliente = 0;
                this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                  this.totalImp = valor * (this.creditoParcelado14 / 100);
                  this.totalParcelamento = valor * (this.parcela5 / 100);
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;
              //Fim do if do Vendedor 
              } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                //Calculos
                //this.parcelamentoAux = 0.0409;
                  this.taxaCliente = valor * (this.parcela5 / 100);
                this.valorAux = valor;
                this.valorRecebido = valor + this.taxaCliente;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                  this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                this.totalPorcentagemV = 0;
                  this.totalPorCliente = ((this.parcela5 / 100) * 100);
                this.totalPorVendedor = this.porcentagemVendedor;
                //console.log(this.valorParcela);
                  this.totalImp = valor * (this.creditoParcelado14 / 100);
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = this.valorRecebido;
              } // fim do if do cliente
              
              }// Fim do if da quantidade de parcelas = 5
              else if(parcelas == 6){ // Inicio do if da quantidade de parcelas = 6
                if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                //Calculos
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                //console.log(this.valorParcela);
                  this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                  this.totalPorcentagemV = ((this.parcela6 / 100) * 100);
                this.totalPorCliente = 0;
                this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                  this.totalImp = valor * (this.creditoParcelado14 / 100);
                  this.totalParcelamento = valor * (this.parcela6 / 100);
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;
              //Fim do if do Vendedor 
              } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                //Calculos
                //this.parcelamentoAux = 0.0409;
                  this.taxaCliente = valor * (this.parcela6 / 100);
                this.valorAux = valor;
                this.valorRecebido = valor + this.taxaCliente;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                //console.log(this.valorParcela);
                  this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                this.totalPorcentagemV = 0;
                  this.totalPorCliente = ((this.parcela6 / 100) * 100);
                this.totalPorVendedor = this.porcentagemVendedor;
                  this.totalImp = valor * (this.creditoParcelado14 / 100);
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = this.valorRecebido;
              } // fim do if do cliente
              
              }// Fim do if da quantidade de parcelas = 6
              else if(parcelas == 7){ // Inicio do if da quantidade de parcelas = 7
                if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                //Calculos
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                //console.log(this.valorParcela);
                  this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                  this.totalPorcentagemV = ((this.parcela7 / 100) * 100);
                this.totalPorCliente = 0;
                this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                  this.totalImp = valor * (this.creditoParcelado14 / 100);
                  this.totalParcelamento = valor * (this.parcela7 / 100);
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;
              //Fim do if do Vendedor 
              } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                //Calculos
                //this.parcelamentoAux = 0.0409;
                  this.taxaCliente = valor * (this.parcela7 / 100);
                this.valorAux = valor;
                this.valorRecebido = valor + this.taxaCliente;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                //console.log(this.valorParcela);
                  this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                this.totalPorcentagemV = 0;
                  this.totalPorCliente = ((this.parcela7 / 100) * 100);
                this.totalPorVendedor = this.porcentagemVendedor;
                  this.totalImp = valor * (this.creditoParcelado14 / 100);
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = this.valorRecebido;
              } // fim do if do cliente
              
              }// Fim do if da quantidade de parcelas = 7
              else if(parcelas == 8){ // Inicio do if da quantidade de parcelas = 8
                if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                //Calculos
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                //console.log(this.valorParcela);
                  this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                  this.totalPorcentagemV = ((this.parcela8 / 100) * 100);
                this.totalPorCliente = 0;
                this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                  this.totalImp = valor * (this.creditoParcelado14 / 100);
                  this.totalParcelamento = valor * (this.parcela8 / 100);
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;
              //Fim do if do Vendedor 
              } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                //Calculos
                //this.parcelamentoAux = 0.0409;
                  this.taxaCliente = valor * (this.parcela8 / 100);
                this.valorAux = valor;
                this.valorRecebido = valor + this.taxaCliente;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                //console.log(this.valorParcela);
                  this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                this.totalPorcentagemV = 0;
                  this.totalPorCliente = ((this.parcela8 / 100) * 100);
                this.totalPorVendedor = this.porcentagemVendedor;
                  this.totalImp = valor * (this.creditoParcelado14 / 100);
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = this.valorRecebido;
              } // fim do if do cliente
              
              }// Fim do if da quantidade de parcelas = 8
              else if(parcelas == 9){ // Inicio do if da quantidade de parcelas = 9
                if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                //Calculos
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                //console.log(this.valorParcela);
                  this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                  this.totalPorcentagemV = ((this.parcela9 / 100) * 100);
                this.totalPorCliente = 0;
                this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                  this.totalImp = valor * (this.creditoParcelado14 / 100);
                  this.totalParcelamento = valor * (this.parcela9 / 100);
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;
              //Fim do if do Vendedor 
              } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                //Calculos
                //this.parcelamentoAux = 0.0409;
                  this.taxaCliente = valor * (this.parcela9 / 100);
                this.valorAux = valor;
                this.valorRecebido = valor + this.taxaCliente;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                //console.log(this.valorParcela);
                  this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                this.totalPorcentagemV = 0;
                this.totalPorCliente = ((this.parcela9 / 100) * 100);
                this.totalPorVendedor = this.porcentagemVendedor;
                  this.totalImp = valor * (this.creditoParcelado14 / 100);
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = this.valorRecebido;
              } // fim do if do cliente
              
              }// Fim do if da quantidade de parcelas = 9
              else if(parcelas == 10){ // Inicio do if da quantidade de parcelas = 10
                if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                //Calculos
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                //console.log(this.valorParcela);
                  this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                  this.totalPorcentagemV = ((this.parcela10 / 100) * 100);
                this.totalPorCliente = 0;
                this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                  this.totalImp = valor * (this.creditoParcelado14 / 100);
                  this.totalParcelamento = valor * (this.parcela10 / 100);
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;
              //Fim do if do Vendedor 
              } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                //Calculos
                //this.parcelamentoAux = 0.0409;
                  this.taxaCliente = valor * (this.parcela10 / 100);
                this.valorAux = valor;
                this.valorRecebido = valor + this.taxaCliente;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                //console.log(this.valorParcela);
                  this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                this.totalPorcentagemV = 0;
                  this.totalPorCliente = ((this.parcela10 / 100) * 100);
                this.totalPorVendedor = this.porcentagemVendedor;
                  this.totalImp = valor * (this.creditoParcelado14 / 100);
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = this.valorRecebido;
              } // fim do if do cliente
              
              }// Fim do if da quantidade de parcelas = 10
              else if(parcelas == 11){ // Inicio do if da quantidade de parcelas = 11
                if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                //Calculos
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                //console.log(this.valorParcela);
                  this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                  this.totalPorcentagemV = ((this.parcela11 / 100) * 100);
                this.totalPorCliente = 0;
                this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                  this.totalImp = valor * (this.creditoParcelado14 / 100);
                  this.totalParcelamento = valor * (this.parcela11 / 100);
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;
              //Fim do if do Vendedor 
              } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                //Calculos
                //this.parcelamentoAux = 0.0409;
                  this.taxaCliente = valor * (this.parcela11 / 100);
                this.valorAux = valor;
                this.valorRecebido = valor + this.taxaCliente;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                //console.log(this.valorParcela);
                  this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                this.totalPorcentagemV = 0;
                  this.totalPorCliente = ((this.parcela11 / 100) * 100);
                this.totalPorVendedor = this.porcentagemVendedor;
                  this.totalImp = valor * (this.creditoParcelado14 / 100);
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = this.valorRecebido;
              } // fim do if do cliente
              
              }// Fim do if da quantidade de parcelas = 11
              else if(parcelas == 12){ // Inicio do if da quantidade de parcelas = 12
                if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                //Calculos
                this.taxaCliente = 0;
                this.valorAux = valor;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                //console.log(this.valorParcela);
                  this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                  this.totalPorcentagemV = ((this.parcela12 / 100) * 100);
                this.totalPorCliente = 0;
                this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                  this.totalImp = valor * (this.creditoParcelado14 / 100);
                  this.totalParcelamento = valor * (this.parcela12 / 100);
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = valor;
              //Fim do if do Vendedor 
              } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                //Calculos
                //this.parcelamentoAux = 0.0409;
                  this.taxaCliente = valor * (this.parcela12 / 100);
                this.valorAux = valor;
                this.valorRecebido = valor + this.taxaCliente;
                this.qtdParcelas = parcelas;
                this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                //console.log(this.valorParcela);
                  this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                this.totalPorcentagemV = 0;
                  this.totalPorCliente = ((this.parcela12 / 100) * 100);
                this.totalPorVendedor = this.porcentagemVendedor;
                  this.totalImp = valor * (this.creditoParcelado14 / 100);
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = valor - this.totalTaxas;
                this.totalCliente = this.valorRecebido;
              } // fim do if do cliente
              
              }// Fim do if da quantidade de parcelas = 12


            }// Fim do if para receber em 14 dias para receber no credito
           }// fim do if para receber em 14 dias
           else if (recebimento == "30") {//Inicio do Recebimento em 30 dias
              if (pagamento == "Credito"){ //Inicio do if do pagamento em credito
                  if(parcelas == 1){ //Inicio do if da quantidade de parcelas 1
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                        
                        this.taxaCliente = 0;
                        this.valorAux = valor;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        //console.log(this.valorParcela);
                      this.porcentagemVendedor = ((this.credito30/100) * 100);
                        this.totalPorcentagemV = 0;
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                      this.totalImp = valor * (this.credito30 / 100);
                        this.totalParcelamento = valor * this.taxaCliente;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = valor - this.totalTaxas;
                        this.totalCliente = valor;

                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                        this.taxaCliente = 0;
                        this.valorAux = valor;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        //console.log(this.valorParcela);
                      this.porcentagemVendedor = ((this.credito30 / 100) * 100);
                        this.totalPorcentagemV = 0;
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor;
                      this.totalImp = valor * (this.credito30 / 100);
                        this.totalParcelamento = valor * this.taxaCliente;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = valor - this.totalTaxas;
                        this.totalCliente = valor;
                  } // fim do if do cliente

                  }//Fim do if da quantidade de parcelas 1
                  else if(parcelas == 2){ // Inicio do if da quantidade de parcelas = 2
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                    //Calculos
                    this.taxaCliente = 0;
                    this.valorAux = valor;
                    this.qtdParcelas = parcelas;
                    this.valorParcela = (this.valorAux/this.qtdParcelas);
                    //console.log(this.valorParcela);
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                      this.totalPorcentagemV = ((this.parcela2 / 100) * 100);
                    this.totalPorCliente = 0;
                    this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                      this.totalImp = valor * (this.creditoParcelado30 / 100);
                      this.totalParcelamento = valor * (this.parcela2 / 100);
                    this.totalTaxas = this.totalImp + this.totalParcelamento;
                    this.total = valor - this.totalTaxas;
                    this.totalCliente = valor;
                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                    //this.parcelamentoAux = 0.0409;
                      this.taxaCliente = valor * (this.parcela2 / 100);
                    this.valorAux = valor;
                    this.valorRecebido = valor + this.taxaCliente;
                    this.qtdParcelas = parcelas;
                    this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                    this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela2 / 100) * 100);
                    this.totalPorVendedor = this.porcentagemVendedor;
                    //console.log(this.valorParcela);
                      this.totalImp = valor * (this.creditoParcelado30 / 100);
                    this.totalParcelamento = valor * 0;
                    this.totalTaxas = this.totalImp + this.totalParcelamento;
                    this.total = valor - this.totalTaxas;
                    this.totalCliente = this.valorRecebido;
                  } // fim do if do cliente
                  
                  }// Fim do if da quantidade de parcelas = 2
                  else if(parcelas == 3){ // Inicio do if da quantidade de parcelas = 3
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                    //Calculos
                    this.taxaCliente = 0;
                    this.valorAux = valor;
                    this.qtdParcelas = parcelas;
                    this.valorParcela = (this.valorAux/this.qtdParcelas);
                    //console.log(this.valorParcela);
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                      this.totalPorcentagemV = ((this.parcela3 / 100) * 100);;
                    this.totalPorCliente = 0;
                    this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                      this.totalImp = valor * (this.creditoParcelado30 / 100);
                      this.totalParcelamento = valor * (this.parcela3 / 100);
                    this.totalTaxas = this.totalImp + this.totalParcelamento;
                    this.total = valor - this.totalTaxas;
                    this.totalCliente = valor;
                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                    //this.parcelamentoAux = 0.0409;
                      this.taxaCliente = valor * (this.parcela3 / 100) ;
                    this.valorAux = valor;
                    this.valorRecebido = valor + this.taxaCliente;
                    this.qtdParcelas = parcelas;
                    this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                    //console.log(this.valorParcela);
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                    this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela3 / 100) * 100);
                    this.totalPorVendedor = this.porcentagemVendedor;
                      this.totalImp = valor * (this.creditoParcelado30 / 100);
                    this.totalParcelamento = valor * 0;
                    this.totalTaxas = this.totalImp + this.totalParcelamento;
                    this.total = valor - this.totalTaxas;
                    this.totalCliente = this.valorRecebido;
                  } // fim do if do cliente
                  
                  }// Fim do if da quantidade de parcelas = 3
                  else if(parcelas == 4){ // Inicio do if da quantidade de parcelas = 4
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                    //Calculos
                    this.taxaCliente = 0;
                    this.valorAux = valor;
                    this.qtdParcelas = parcelas;
                    this.valorParcela = (this.valorAux/this.qtdParcelas);
                    //console.log(this.valorParcela);
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                      this.totalPorcentagemV = ((this.parcela4 / 100) * 100);
                    this.totalPorCliente = 0;
                    this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                      this.totalImp = valor * (this.creditoParcelado30 / 100);
                      this.totalParcelamento = valor * (this.parcela4 / 100);
                    this.totalTaxas = this.totalImp + this.totalParcelamento;
                    this.total = valor - this.totalTaxas;
                    this.totalCliente = valor;
                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                    //this.parcelamentoAux = 0.0409;
                      this.taxaCliente = valor * (this.parcela4 / 100)  ;
                    this.valorAux = valor;
                    this.valorRecebido = valor + this.taxaCliente;
                    this.qtdParcelas = parcelas;
                    this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                    //console.log(this.valorParcela);
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                    this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela4 / 100) * 100);
                    this.totalPorVendedor = this.porcentagemVendedor;
                      this.totalImp = valor * (this.creditoParcelado30 / 100);
                    this.totalParcelamento = valor * 0;
                    this.totalTaxas = this.totalImp + this.totalParcelamento;
                    this.total = valor - this.totalTaxas;
                    this.totalCliente = this.valorRecebido;
                  } // fim do if do cliente
                  
                  }// Fim do if da quantidade de parcelas = 4
                  else if(parcelas == 5){ // Inicio do if da quantidade de parcelas = 5
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                    //Calculos
                    this.taxaCliente = 0;
                    this.valorAux = valor;
                    this.qtdParcelas = parcelas;
                    this.valorParcela = (this.valorAux/this.qtdParcelas);
                    //console.log(this.valorParcela);
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                      this.totalPorcentagemV = ((this.parcela5 / 100) * 100);
                    this.totalPorCliente = 0;
                    this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                      this.totalImp = valor * (this.creditoParcelado30 / 100);
                      this.totalParcelamento = valor * (this.parcela5 / 100);
                    this.totalTaxas = this.totalImp + this.totalParcelamento;
                    this.total = valor - this.totalTaxas;
                    this.totalCliente = valor;
                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                    //this.parcelamentoAux = 0.0409;
                      this.taxaCliente = valor * (this.parcela5 / 100);
                    this.valorAux = valor;
                    this.valorRecebido = valor + this.taxaCliente;
                    this.qtdParcelas = parcelas;
                    this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                    //console.log(this.valorParcela);
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                    this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela5 / 100) * 100);
                    this.totalPorVendedor = this.porcentagemVendedor;
                      this.totalImp = valor * (this.creditoParcelado30 / 100);
                    this.totalParcelamento = valor * 0;
                    this.totalTaxas = this.totalImp + this.totalParcelamento;
                    this.total = valor - this.totalTaxas;
                    this.totalCliente = this.valorRecebido;
                  } // fim do if do cliente
                  
                  }// Fim do if da quantidade de parcelas = 5
                  else if(parcelas == 6){ // Inicio do if da quantidade de parcelas = 6
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                    //Calculos
                    this.taxaCliente = 0;
                    this.valorAux = valor;
                    this.qtdParcelas = parcelas;
                    this.valorParcela = (this.valorAux/this.qtdParcelas);
                    //console.log(this.valorParcela);
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                      this.totalPorcentagemV = ((this.parcela6 / 100) * 100);
                    this.totalPorCliente = 0;
                    this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                      this.totalImp = valor * (this.creditoParcelado30 / 100);
                      this.totalParcelamento = valor * (this.parcela6 / 100);
                    this.totalTaxas = this.totalImp + this.totalParcelamento;
                    this.total = valor - this.totalTaxas;
                    this.totalCliente = valor;
                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                    //this.parcelamentoAux = 0.0409;
                      this.taxaCliente = valor * (this.parcela6 / 100);
                    this.valorAux = valor;
                    this.valorRecebido = valor + this.taxaCliente;
                    this.qtdParcelas = parcelas;
                    this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                    //console.log(this.valorParcela);
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                    this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela6 / 100) * 100);
                    this.totalPorVendedor = this.porcentagemVendedor;
                      this.totalImp = valor * (this.creditoParcelado30 / 100);
                    this.totalParcelamento = valor * 0;
                    this.totalTaxas = this.totalImp + this.totalParcelamento;
                    this.total = valor - this.totalTaxas;
                    this.totalCliente = this.valorRecebido;
                  } // fim do if do cliente
                  
                  }// Fim do if da quantidade de parcelas = 6
                  else if(parcelas == 7){ // Inicio do if da quantidade de parcelas = 7
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                    //Calculos
                    this.taxaCliente = 0;
                    this.valorAux = valor;
                    this.qtdParcelas = parcelas;
                    this.valorParcela = (this.valorAux/this.qtdParcelas);
                    //console.log(this.valorParcela);
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                      this.totalPorcentagemV = ((this.parcela7 / 100) * 100);
                    this.totalPorCliente = 0;
                    this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                      this.totalImp = valor * (this.creditoParcelado30 / 100);
                      this.totalParcelamento = valor * (this.parcela7 / 100);
                    this.totalTaxas = this.totalImp + this.totalParcelamento;
                    this.total = valor - this.totalTaxas;
                    this.totalCliente = valor;
                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                    //this.parcelamentoAux = 0.0409;
                      this.taxaCliente = valor * (this.parcela7 / 100);
                    this.valorAux = valor;
                    this.valorRecebido = valor + this.taxaCliente;
                    this.qtdParcelas = parcelas;
                    this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                    //console.log(this.valorParcela);
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                    this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela7 / 100) * 100);
                    this.totalPorVendedor = this.porcentagemVendedor;
                      this.totalImp = valor * (this.creditoParcelado30 / 100);
                    this.totalParcelamento = valor * 0;
                    this.totalTaxas = this.totalImp + this.totalParcelamento;
                    this.total = valor - this.totalTaxas;
                    this.totalCliente = this.valorRecebido;
                  } // fim do if do cliente
                  
                  }// Fim do if da quantidade de parcelas = 7
                  else if(parcelas == 8){ // Inicio do if da quantidade de parcelas = 8
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                    //Calculos
                    this.taxaCliente = 0;
                    this.valorAux = valor;
                    this.qtdParcelas = parcelas;
                    this.valorParcela = (this.valorAux/this.qtdParcelas);
                    //console.log(this.valorParcela);
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                      this.totalPorcentagemV = ((this.parcela8 / 100) * 100);
                    this.totalPorCliente = 0;
                    this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                      this.totalImp = valor * (this.creditoParcelado30 / 100);
                      this.totalParcelamento = valor * (this.parcela8 / 100);
                    this.totalTaxas = this.totalImp + this.totalParcelamento;
                    this.total = valor - this.totalTaxas;
                    this.totalCliente = valor;
                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                    //this.parcelamentoAux = 0.0409;
                      this.taxaCliente = valor * (this.parcela8 / 100);
                    this.valorAux = valor;
                    this.valorRecebido = valor + this.taxaCliente;
                    this.qtdParcelas = parcelas;
                    this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                    //console.log(this.valorParcela);
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                    this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela8 / 100) * 100);
                    this.totalPorVendedor = this.porcentagemVendedor;
                      this.totalImp = valor * (this.creditoParcelado30 / 100);
                    this.totalParcelamento = valor * 0;
                    this.totalTaxas = this.totalImp + this.totalParcelamento;
                    this.total = valor - this.totalTaxas;
                    this.totalCliente = this.valorRecebido;
                  } // fim do if do cliente
                  
                  }// Fim do if da quantidade de parcelas = 8
                  else if(parcelas == 9){ // Inicio do if da quantidade de parcelas = 9
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                    //Calculos
                    this.taxaCliente = 0;
                    this.valorAux = valor;
                    this.qtdParcelas = parcelas;
                    this.valorParcela = (this.valorAux/this.qtdParcelas);
                    //console.log(this.valorParcela);
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                      this.totalPorcentagemV = ((this.parcela9 / 100) * 100);
                    this.totalPorCliente = 0;
                    this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                      this.totalImp = valor * (this.creditoParcelado30 / 100);
                      this.totalParcelamento = valor * (this.parcela9 / 100);
                    this.totalTaxas = this.totalImp + this.totalParcelamento;
                    this.total = valor - this.totalTaxas;
                    this.totalCliente = valor;
                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                    //this.parcelamentoAux = 0.0409;
                      this.taxaCliente = valor * (this.parcela9 / 100);
                    this.valorAux = valor;
                    this.qtdParcelas = parcelas;
                    this.valorRecebido = valor + this.taxaCliente;
                    this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                    //console.log(this.valorParcela);
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                    this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela9 / 100) * 100);
                    this.totalPorVendedor = this.porcentagemVendedor;
                      this.totalImp = valor * (this.creditoParcelado30 / 100);
                    this.totalParcelamento = valor * 0;
                    this.totalTaxas = this.totalImp + this.totalParcelamento;
                    this.total = valor - this.totalTaxas;
                    this.totalCliente = this.valorRecebido;
                  } // fim do if do cliente
                  
                  }// Fim do if da quantidade de parcelas = 9
                  else if(parcelas == 10){ // Inicio do if da quantidade de parcelas = 10
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                    //Calculos
                    this.taxaCliente = 0;
                    this.valorAux = valor;
                    this.qtdParcelas = parcelas;
                    this.valorParcela = (this.valorAux/this.qtdParcelas);
                    //console.log(this.valorParcela);
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                      this.totalPorcentagemV = ((this.parcela10 / 100) * 100);
                    this.totalPorCliente = 0;
                    this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                      this.totalImp = valor * (this.creditoParcelado30 / 100);
                      this.totalParcelamento = valor * (this.parcela10 / 100);
                    this.totalTaxas = this.totalImp + this.totalParcelamento;
                    this.total = valor - this.totalTaxas;
                    this.totalCliente = valor;
                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                    //this.parcelamentoAux = 0.0409;
                      this.taxaCliente = valor * (this.parcela10 / 100);
                    this.valorAux = valor;
                    this.valorRecebido = valor + this.taxaCliente;
                    this.qtdParcelas = parcelas;
                    this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                    //console.log(this.valorParcela);
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                    this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela10 / 100) * 100);
                    this.totalPorVendedor = this.porcentagemVendedor;
                      this.totalImp = valor * (this.creditoParcelado30 / 100);
                    this.totalParcelamento = valor * 0;
                    this.totalTaxas = this.totalImp + this.totalParcelamento;
                    this.total = valor - this.totalTaxas;
                    this.totalCliente = this.valorRecebido;
                  } // fim do if do cliente
                  
                  }// Fim do if da quantidade de parcelas = 10
                  else if(parcelas == 11){ // Inicio do if da quantidade de parcelas = 11
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                    //Calculos
                    this.taxaCliente = 0;
                    this.valorAux = valor;
                    this.qtdParcelas = parcelas;
                    this.valorParcela = (this.valorAux/this.qtdParcelas);
                    //console.log(this.valorParcela);
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                      this.totalPorcentagemV = ((this.parcela11 / 100) * 100);
                    this.totalPorCliente = 0;
                    this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                      this.totalImp = valor * (this.creditoParcelado30 / 100);
                      this.totalParcelamento = valor * (this.parcela11 / 100);
                    this.totalTaxas = this.totalImp + this.totalParcelamento;
                    this.total = valor - this.totalTaxas;
                    this.totalCliente = valor;
                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                    //this.parcelamentoAux = 0.0409;
                      this.taxaCliente = valor * (this.parcela11 / 100);
                    this.valorAux = valor;
                    this.valorRecebido = valor + this.taxaCliente;
                    this.qtdParcelas = parcelas;
                    this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                    //console.log(this.valorParcela);
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                    this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela11 / 100) * 100);
                    this.totalPorVendedor = this.porcentagemVendedor;
                      this.totalImp = valor * (this.creditoParcelado30 / 100);
                    this.totalParcelamento = valor * 0;
                    this.totalTaxas = this.totalImp + this.totalParcelamento;
                    this.total = valor - this.totalTaxas;
                    this.totalCliente = this.valorRecebido;
                  } // fim do if do cliente
                  
                  }// Fim do if da quantidade de parcelas = 11
                  else if(parcelas == 12){ // Inicio do if da quantidade de parcelas = 12
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                    //Calculos
                    this.taxaCliente = 0;
                    this.valorAux = valor;
                    this.qtdParcelas = parcelas;
                    this.valorParcela = (this.valorAux/this.qtdParcelas);
                    //console.log(this.valorParcela);
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                      this.totalPorcentagemV = ((this.parcela12 / 100) * 100);
                    this.totalPorCliente = 0;
                    this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                      this.totalImp = valor * (this.creditoParcelado30 / 100);
                      this.totalParcelamento = valor * (this.parcela12 / 100);
                    this.totalTaxas = this.totalImp + this.totalParcelamento;
                    this.total = valor - this.totalTaxas;
                    this.totalCliente = valor;
                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                    //this.parcelamentoAux = 0.0409;
                      this.taxaCliente = valor * (this.parcela12 / 100);
                    this.valorAux = valor;
                    this.valorRecebido = valor + this.taxaCliente;
                    this.qtdParcelas = parcelas;
                    this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                    //console.log(this.valorParcela);
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                    this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela12 / 100) * 100);
                    this.totalPorVendedor = this.porcentagemVendedor;
                      this.totalImp = valor * (this.creditoParcelado30 / 100);
                    this.totalParcelamento = valor * 0;
                    this.totalTaxas = this.totalImp + this.totalParcelamento;
                    this.total = valor - this.totalTaxas;
                    this.totalCliente = this.valorRecebido;
                  } // fim do if do cliente
                  
                  }// Fim do if da quantidade de parcelas = 12

              }//fim do if do pagamento em credito

           }//Fim do if do Recebimento em 30 dias
        }//Fim do if da maquina Point
      //Calculo Reverso para o mercado pago 
      }else if(tipo == "Reverso"){
        if (maquina == "Point") {//Inicio do if da maquina Point
          if (recebimento == "Hora") {// Inicio do if do recebimento
            if (pagamento == "Debito") { //Inicio do if do tipo de pagamento
              if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                this.taxasAux = valor * ((this.debito/100) + 0.0006);
                this.taxaCliente = 0;
                this.valorAux = valor + this.taxasAux;
                this.qtdParcelas = 1;
                this.valorParcela = (this.valorAux/this.qtdParcelas);
                this.porcentagemVendedor = ((this.debito/100) * 100);
                this.totalPorVendedor = this.porcentagemVendedor;
                this.totalPorcentagemV = 0;
                this.totalPorCliente = this.taxaCliente;
                this.totalImp = this.taxasAux;
                this.totalParcelamento = valor * 0;
                this.totalTaxas = this.totalImp + this.totalParcelamento;
                this.total = this.valorAux - this.totalTaxas;
                this.totalCliente = this.valorAux;
              } // Fim do if do parcelamento vendedor e entra no else
            } // Fim do if do tipo de pagamento e entra no else no tipo de credito
              else if (pagamento == "Credito") { // Inicio do if do pagamento do tipo credito
                  if(parcelas == 1){ //Inicio do if da quantidade de parcelas 1
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                        this.taxasAux = valor * ((this.creditoHora / 100) + 0.0023);
                        this.taxaCliente = 0;
                        this.valorAux = valor + this.taxasAux;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        this.porcentagemVendedor = ((this.creditoHora / 100) * 100);
                        this.totalPorVendedor = this.porcentagemVendedor;
                        this.totalPorCliente = this.taxaCliente;
                        this.totalPorcentagemV = 0;
                        //console.log(this.valorParcela);
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = valor * 0;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;
                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                        //Variaveis auxiliar "taxasAux e valorAux" para calcular o reverso
                      this.taxasAux = valor * ((this.creditoHora / 100) + 0.0024);
                        this.valorAux = valor + this.taxasAux;
                        this.taxaCliente = this.valorAux * 0;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        this.porcentagemVendedor = ((this.creditoHora / 100) * 100);
                        this.totalPorVendedor = this.porcentagemVendedor;
                        this.totalPorCliente = this.taxaCliente;
                        this.totalPorcentagemV = 0;
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = valor * 0;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;
                  } // fim do if do cliente

                  }//Fim do if da quantidade de parcelas 1
                  else if(parcelas == 2){ //Inicio do if da quantidade de parcelas 2
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                        //this.valorReal = valor;
                        this.taxasAux = valor * ((this.creditoParceladoHora / 100) + 0.0055);
                      this.taxaParcelamentoAux = valor * ((this.parcela2 / 100) + 0.0043);
                        this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                        this.totalPorcentagemV = ((this.parcela2 / 100) * 100);
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;                        
                        this.taxaCliente = 0;
                        this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        //console.log(this.valorParcela);
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = this.taxaParcelamentoAux;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;


                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                      this.taxasAux = valor * ((this.creditoParceladoHora / 100) + 0.003);
                      this.taxaParcelamentoAux = valor * 0; 

                      this.taxaCliente = valor * ((this.parcela2 / 100) + 0.0023);
                      this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                      this.valorRecebido = this.valorAux + this.taxaCliente;
                      this.qtdParcelas = parcelas;
                      this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                      //console.log(this.valorParcela);
                      this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                      this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela2 / 100) * 100);
                      this.totalPorVendedor = this.porcentagemVendedor;
                      this.totalImp = this.taxasAux;
                      this.totalParcelamento = valor * 0;
                      this.totalTaxas = this.totalImp + this.totalParcelamento;
                      this.total = this.valorAux - this.totalTaxas;
                      this.totalCliente = this.valorRecebido;


                  } // fim do if do cliente

                }//Fim do if da quantidade de parcelas 2
            else if(parcelas == 3){ //Inicio do if da quantidade de parcelas 3
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                        
                        //this.valorReal = valor;
                        this.taxasAux = valor * ((this.creditoParceladoHora / 100) + 0.0064);
                      this.taxaParcelamentoAux = valor * ((this.parcela3 / 100) + 0.0065);
                        this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                        this.totalPorcentagemV = ((this.parcela3 / 100) * 100);
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                        this.taxaCliente = 0;
                        this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        //console.log(this.valorParcela);
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = this.taxaParcelamentoAux;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;


                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                      this.taxasAux = valor * ((this.creditoParceladoHora / 100) + 0.003);
                      this.taxaParcelamentoAux = valor * 0; 

                      this.taxaCliente = valor * ((this.parcela3 / 100) + 0.0031);
                      this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                      this.valorRecebido = this.valorAux + this.taxaCliente;
                      this.qtdParcelas = parcelas;
                      this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                      this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                      this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela3 / 100) * 100);
                      this.totalPorVendedor = this.porcentagemVendedor;
                      this.totalImp = this.taxasAux;
                      this.totalParcelamento = valor * 0;
                      this.totalTaxas = this.totalImp + this.totalParcelamento;
                      this.total = this.valorAux - this.totalTaxas;
                      this.totalCliente = this.valorRecebido;


                  } // fim do if do cliente

                }//Fim do if da quantidade de parcelas 3
               else if(parcelas == 4){ //Inicio do if da quantidade de parcelas 4
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                        
                        //this.valorReal = valor;
                        this.taxasAux = valor * ((this.creditoParceladoHora / 100) + 0.0073);
                      this.taxaParcelamentoAux = valor * ((this.parcela4 / 100) + 0.0092);
                        this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                        this.totalPorcentagemV = ((this.parcela4 / 100) * 100);
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                        this.taxaCliente = 0;
                        this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = this.taxaParcelamentoAux;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;


                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                      this.taxasAux = valor * ((this.creditoParceladoHora / 100) + 0.003);
                      this.taxaParcelamentoAux = valor * 0;
                      this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                      this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela4 / 100) * 100);
                      this.totalPorVendedor = this.porcentagemVendedor; 

                      this.taxaCliente = valor * ((this.parcela4 / 100) + 0.0037);
                      this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                      this.valorRecebido = this.valorAux + this.taxaCliente;
                      this.qtdParcelas = parcelas;
                      this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                      //console.log(this.valorParcela);
                      this.totalImp = this.taxasAux;
                      this.totalParcelamento = valor * 0;
                      this.totalTaxas = this.totalImp + this.totalParcelamento;
                      this.total = this.valorAux - this.totalTaxas;
                      this.totalCliente = this.valorRecebido;


                  } // fim do if do cliente

                }//Fim do if da quantidade de parcelas 4
               else if(parcelas == 5){ //Inicio do if da quantidade de parcelas 5
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                        //this.valorReal = valor;
                        this.taxasAux = valor * ((this.creditoParceladoHora / 100) + 0.0081);
                        this.taxaParcelamentoAux = valor * ((this.parcela5 / 100) + 0.0122);

                        this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                        this.totalPorcentagemV = ((this.parcela5 / 100) * 100);
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                        
                        this.taxaCliente = 0;
                        this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        //console.log(this.valorParcela);
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = this.taxaParcelamentoAux;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;
                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                      this.taxasAux = valor * ((this.creditoParceladoHora / 100) + 0.003);
                      this.taxaParcelamentoAux = valor * 0;   
                      this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                      this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela5 / 100) * 100);
                      this.totalPorVendedor = this.porcentagemVendedor;
                      this.taxaCliente = valor * ((this.parcela5 / 100) + 0.0045);
                      this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                      this.valorRecebido = this.valorAux + this.taxaCliente;
                      this.qtdParcelas = parcelas;
                      this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                      //console.log(this.valorParcela);
                      this.totalImp = this.taxasAux;
                      this.totalParcelamento = valor * 0;
                      this.totalTaxas = this.totalImp + this.totalParcelamento;
                      this.total = this.valorAux - this.totalTaxas;
                      this.totalCliente = this.valorRecebido;


                  } // fim do if do cliente

                }//Fim do if da quantidade de parcelas 5
               else if(parcelas == 6){ //Inicio do if da quantidade de parcelas 6
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                        
                        //this.valorReal = valor;
                        this.taxasAux = valor * ((this.creditoParceladoHora / 100) + 0.009);
                        this.taxaParcelamentoAux = valor * ((this.parcela6 / 100) + 0.0156);
                        //console.log(valor);
                        this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                        this.totalPorcentagemV = ((this.parcela6 / 100) * 100);
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                        this.taxaCliente = 0;
                        this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        //console.log(this.valorParcela);
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = this.taxaParcelamentoAux;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;


                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                      this.taxasAux = valor * ((this.creditoParceladoHora / 100) + 0.003);
                      this.taxaParcelamentoAux = valor * 0; 
                      this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                      this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela6 / 100) * 100);
                      this.totalPorVendedor = this.porcentagemVendedor;
                      this.taxaCliente = valor * ((this.parcela6 / 100) + 0.0052);
                      this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                      this.valorRecebido = this.valorAux + this.taxaCliente;
                      this.qtdParcelas = parcelas;
                      this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                      //console.log(this.valorParcela);
                      this.totalImp = this.taxasAux;
                      this.totalParcelamento = valor * 0;
                      this.totalTaxas = this.totalImp + this.totalParcelamento;
                      this.total = this.valorAux - this.totalTaxas;
                      this.totalCliente = this.valorRecebido;


                  } // fim do if do cliente

                }//Fim do if da quantidade de parcelas 6
            else if(parcelas == 7){ //Inicio do if da quantidade de parcelas 7
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                        
                        //this.valorReal = valor;
                      this.taxasAux = valor * ((this.creditoParceladoHora / 100) + 0.0099);
                      this.taxaParcelamentoAux = valor * ((this.parcela7 / 100) + 0.0194); 

                        this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                        this.totalPorcentagemV = ((this.parcela7 / 100) * 100);
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                        
                        this.taxaCliente = 0;
                        this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        //console.log(this.valorParcela);
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = this.taxaParcelamentoAux;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;


                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                      this.taxasAux = valor * ((this.creditoParceladoHora / 100) + 0.003);
                      this.taxaParcelamentoAux = valor * 0; 
                      this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                      this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela7 / 100) * 100);
                      this.totalPorVendedor = this.porcentagemVendedor;
                      this.taxaCliente = valor * ((this.parcela7 / 100) + 0.0059); 
                      this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                      this.valorRecebido = this.valorAux + this.taxaCliente;
                      this.qtdParcelas = parcelas;
                      this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                      //console.log(this.valorParcela);
                      this.totalImp = this.taxasAux;
                      this.totalParcelamento = valor * 0;
                      this.totalTaxas = this.totalImp + this.totalParcelamento;
                      this.total = this.valorAux - this.totalTaxas;
                      this.totalCliente = this.valorRecebido;


                  } // fim do if do cliente

                }//Fim do if da quantidade de parcelas 7
            else if(parcelas == 8){ //Inicio do if da quantidade de parcelas 8
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                        
                        //this.valorReal = valor;
                        this.taxasAux = valor * ((this.creditoParceladoHora / 100) + 0.0108);
                        this.taxaParcelamentoAux = valor * ((this.parcela8 / 100) + 0.0237);
                        this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                        this.totalPorcentagemV = ((this.parcela8 / 100) * 100);
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                        this.taxaCliente = 0;
                        this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        //console.log(this.valorParcela);
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = this.taxaParcelamentoAux;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;

                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                      this.taxasAux = valor * ((this.creditoParceladoHora / 100) + 0.003);
                      this.taxaParcelamentoAux = valor * 0; 
                      this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                      this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela8 / 100) * 100);
                      this.totalPorVendedor = this.porcentagemVendedor;
                      this.taxaCliente = valor * ((this.parcela8 / 100) + 0.0065);
                      this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                      this.valorRecebido = this.valorAux + this.taxaCliente;
                      this.qtdParcelas = parcelas;
                      this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                      //console.log(this.valorParcela);
                      this.totalImp = this.taxasAux;
                      this.totalParcelamento = valor * 0;
                      this.totalTaxas = this.totalImp + this.totalParcelamento;
                      this.total = this.valorAux - this.totalTaxas;
                      this.totalCliente = this.valorRecebido;
                  } // fim do if do cliente
                }//Fim do if da quantidade de parcelas 8
            else if(parcelas == 9){ //Inicio do if da quantidade de parcelas 9
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                        
                        //this.valorReal = valor;
                        this.taxasAux = valor * ((this.creditoParceladoHora / 100) + 0.0118);
                        this.taxaParcelamentoAux = valor * ((this.parcela9 / 100) + 0.0282);
                        this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                        this.totalPorcentagemV = ((this.parcela9 / 100) * 100);
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                        this.taxaCliente = 0;
                        this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        //console.log(this.valorParcela);
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = this.taxaParcelamentoAux;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;
                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                      this.taxasAux = valor * ((this.creditoParceladoHora / 100) + 0.003);
                      this.taxaParcelamentoAux = valor * 0; 
                      this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                      this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela9 / 100) * 100);
                      this.totalPorVendedor = this.porcentagemVendedor;
                      this.taxaCliente = valor * ((this.parcela9 / 100) + 0.0072);
                      this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                      this.valorRecebido = this.valorAux + this.taxaCliente;
                      this.qtdParcelas = parcelas;
                      this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                      //console.log(this.valorParcela);
                      this.totalImp = this.taxasAux;
                      this.totalParcelamento = valor * 0;
                      this.totalTaxas = this.totalImp + this.totalParcelamento;
                      this.total = this.valorAux - this.totalTaxas;
                      this.totalCliente = this.valorRecebido;
                  } // fim do if do cliente
                }//Fim do if da quantidade de parcelas 9
              else if(parcelas == 10){ //Inicio do if da quantidade de parcelas 10
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                        
                        //this.valorReal = valor;
                        this.taxasAux = valor * ((this.creditoParceladoHora / 100) + 0.0127);
                        this.taxaParcelamentoAux = valor * ((this.parcela10 / 100) + 0.0332);
                        this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                        this.totalPorcentagemV = ((this.parcela10 / 100) * 100);
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                        this.taxaCliente = 0;
                        this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        //console.log(this.valorParcela);
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = this.taxaParcelamentoAux;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;
                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                      this.taxasAux = valor * ((this.creditoParceladoHora / 100) + 0.003);
                      this.taxaParcelamentoAux = valor * 0; 
                      this.taxaCliente = valor * ((this.parcela10 / 100) + 0.0078);
                      this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                      this.valorRecebido = this.valorAux + this.taxaCliente;
                      this.qtdParcelas = parcelas;
                      this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                      //console.log(this.valorParcela);
                      this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                      this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela10 / 100) * 100);
                      this.totalPorVendedor = this.porcentagemVendedor;
                      this.totalImp = this.taxasAux;
                      this.totalParcelamento = valor * 0;
                      this.totalTaxas = this.totalImp + this.totalParcelamento;
                      this.total = this.valorAux - this.totalTaxas;
                      this.totalCliente = this.valorRecebido;
                  } // fim do if do cliente
                }//Fim do if da quantidade de parcelas 10
            else if(parcelas == 11){ //Inicio do if da quantidade de parcelas 11
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                        //this.valorReal = valor;
                      this.taxasAux = valor * ((this.creditoParceladoHora / 100) + 0.0136);
                      this.taxaParcelamentoAux = valor * ((this.parcela11 / 100) + 0.0385);
                        this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                        this.totalPorcentagemV = ((this.parcela11 / 100) * 100);
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                        this.taxaCliente = 0;
                        this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        //console.log(this.valorParcela);
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = this.taxaParcelamentoAux;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;


                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                      this.taxasAux = valor * ((this.creditoParceladoHora / 100) + 0.003);
                      this.taxaParcelamentoAux = valor * 0; 

                      this.taxaCliente = valor * ((this.parcela11 / 100) + 0.0085);
                      this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                      this.valorRecebido = this.valorAux + this.taxaCliente;
                      this.qtdParcelas = parcelas;
                      this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                      //console.log(this.valorParcela);
                      this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                      this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela11 / 100) * 100);
                      this.totalPorVendedor = this.porcentagemVendedor;
                      this.totalImp = this.taxasAux;
                      this.totalParcelamento = valor * 0;
                      this.totalTaxas = this.totalImp + this.totalParcelamento;
                      this.total = this.valorAux - this.totalTaxas;
                      this.totalCliente = this.valorRecebido;

                  } // fim do if do cliente

                }//Fim do if da quantidade de parcelas 11
            else if(parcelas == 12){ //Inicio do if da quantidade de parcelas 12
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                        
                        //this.valorReal = valor;
                        this.taxasAux = valor * ((this.creditoParceladoHora / 100) + 0.0145);
                        this.taxaParcelamentoAux = valor * ((this.parcela12 / 100) + 0.0442);
                        this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                        this.totalPorcentagemV = ((this.parcela12 / 100) * 100);
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                        this.taxaCliente = 0;
                        this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        //console.log(this.valorParcela);
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = this.taxaParcelamentoAux;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;


                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                      this.taxasAux = valor * ((this.creditoParceladoHora / 100) + 0.003);
                      this.taxaParcelamentoAux = valor * 0; 

                      this.taxaCliente = valor * ((this.parcela12 / 100) + 0.0091);
                      this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                      this.valorRecebido = this.valorAux + this.taxaCliente;
                      this.qtdParcelas = parcelas;
                      this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                      //console.log(this.valorParcela);
                      this.porcentagemVendedor = ((this.creditoParceladoHora / 100) * 100);
                      this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela12 / 100) * 100);
                      this.totalPorVendedor = this.porcentagemVendedor;
                      this.totalImp = this.taxasAux;
                      this.totalParcelamento = valor * 0;
                      this.totalTaxas = this.totalImp + this.totalParcelamento;
                      this.total = this.valorAux - this.totalTaxas;
                      this.totalCliente = this.valorRecebido;

                  } // fim do if do cliente

                }//Fim do if da quantidade de parcelas 12

              } // Fim do If do pagamento do tipo credito
          } else if (recebimento == "14"){ // Inicio do if do Recebimento em 14 dias
                  if (pagamento == "Credito") { // Inicio do if do pagamento do tipo credito
                    if(parcelas == 1){ //Inicio do if da quantidade de parcelas 1
                      if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                        
                        this.taxasAux = valor * ((this.credito14 / 100) + 0.0015);
                   
                        this.taxaCliente = 0;
                        this.valorAux = valor + this.taxasAux;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        this.porcentagemVendedor = ((this.credito14 / 100) * 100);
                        this.totalPorVendedor = this.porcentagemVendedor;
                        this.totalPorCliente = this.taxaCliente;
                        this.totalPorcentagemV = 0;
                        //console.log(this.valorParcela);
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = valor * 0;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;

                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                        //Variaveis auxiliar "taxasAux e valorAux" para calcular o reverso
                        this.taxasAux = valor * ((this.credito14 / 100) + 0.0015);
                        this.valorAux = valor + this.taxasAux;
                
                        this.taxaCliente = this.valorAux * 0;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        //console.log(this.valorParcela);
                        this.porcentagemVendedor = ((this.credito14 / 100) * 100);
                        this.totalPorVendedor = this.porcentagemVendedor;
                        this.totalPorCliente = this.taxaCliente;
                        this.totalPorcentagemV = 0;
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = valor * 0;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;
                  } // fim do if do cliente

                  }//Fim do if da quantidade de parcelas 1
                   else if(parcelas == 2){ //Inicio do if da quantidade de parcelas 2
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                        
                        //this.valorReal = valor;
                      this.taxasAux = valor * ((this.creditoParcelado14 / 100) + 0.004);
                      this.taxaParcelamentoAux = valor * ((this.parcela2 / 100) + 0.0038);
                      
                      this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                        this.totalPorcentagemV = ((this.parcela2 / 100) * 100);
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                        
                        this.taxaCliente = 0;
                        this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        //console.log(this.valorParcela);
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = this.taxaParcelamentoAux;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;


                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                      this.taxasAux = valor * ((this.creditoParcelado14 / 100) + 0.002);
                      this.taxaParcelamentoAux = valor * 0; 
                      this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                      this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela2 / 100) * 100);
                      this.totalPorVendedor = this.porcentagemVendedor;
                      this.taxaCliente = valor * ((this.parcela2 / 100) + 0.0019);
                      this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                      this.valorRecebido = this.valorAux + this.taxaCliente;
                      this.qtdParcelas = parcelas;
                      this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                      //console.log(this.valorParcela);
                      this.totalImp = this.taxasAux;
                      this.totalParcelamento = this.taxaParcelamentoAux;
                      this.totalTaxas = this.totalImp + this.totalParcelamento;
                      this.total = this.valorAux - this.totalTaxas;
                      this.totalCliente = this.valorRecebido;


                  } // fim do if do cliente

                }//Fim do if da quantidade de parcelas 2
                else if(parcelas == 3){ //Inicio do if da quantidade de parcelas 3
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                        
                        //this.valorReal = valor;
                      this.taxasAux = valor * ((this.creditoParcelado14 / 100) + 0.0047);
                      this.taxaParcelamentoAux = valor * ((this.parcela3 / 100) + 0.0059);
                        
                      this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                        this.totalPorcentagemV = ((this.parcela3 / 100) * 100);
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                        
                        this.taxaCliente = 0;
                        this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        //console.log(this.valorParcela);
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = this.taxaParcelamentoAux;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;


                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                      this.taxasAux = valor * ((this.creditoParcelado14 / 100) + 0.002);
                      this.taxaParcelamentoAux = valor * 0; 
                      this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                      this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela3 / 100) * 100);
                      this.totalPorVendedor = this.porcentagemVendedor;
                      this.taxaCliente = valor * ((this.parcela3 / 100) + 0.0025);
                      this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                      this.valorRecebido = this.valorAux + this.taxaCliente;
                      this.qtdParcelas = parcelas;
                      this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                      //console.log(this.valorParcela);
                      this.totalImp = this.taxasAux;
                      this.totalParcelamento = this.taxaParcelamentoAux;
                      this.totalTaxas = this.totalImp + this.totalParcelamento;
                      this.total = this.valorAux - this.totalTaxas;
                      this.totalCliente = this.valorRecebido;


                  } // fim do if do cliente

                }//Fim do if da quantidade de parcelas 3
                else if(parcelas == 4){ //Inicio do if da quantidade de parcelas 4
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                        
                        //this.valorReal = valor;
                      this.taxasAux = valor * ((this.creditoParcelado14 / 100) + 0.0054);
                      this.taxaParcelamentoAux = valor * ((this.parcela4 / 100) + 0.0084);
                        
                      this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                      this.totalPorcentagemV = ((this.parcela4 / 100) * 100);
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                        
                        this.taxaCliente = 0;
                        this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        //console.log(this.valorParcela);
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = this.taxaParcelamentoAux;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;


                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                      this.taxasAux = valor * ((this.creditoParcelado14 / 100) + 0.002);
                      this.taxaParcelamentoAux = valor * 0; 
                      this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                      this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela4 / 100) * 100);
                      this.totalPorVendedor = this.porcentagemVendedor;
                      this.taxaCliente = valor * ((this.parcela4 / 100) + 0.0031);
                      this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                      this.valorRecebido = this.valorAux + this.taxaCliente;
                      this.qtdParcelas = parcelas;
                      this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                      //console.log(this.valorParcela);
                      this.totalImp = this.taxasAux;
                      this.totalParcelamento = this.taxaParcelamentoAux;
                      this.totalTaxas = this.totalImp + this.totalParcelamento;
                      this.total = this.valorAux - this.totalTaxas;
                      this.totalCliente = this.valorRecebido;


                  } // fim do if do cliente

                }//Fim do if da quantidade de parcelas 4
                else if(parcelas == 5){ //Inicio do if da quantidade de parcelas 5
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                        
                        //this.valorReal = valor;
                      this.taxasAux = valor * ((this.creditoParcelado14 / 100) + 0.0062);
                      this.taxaParcelamentoAux = valor * ((this.parcela5 / 100) + 0.0112);
                        
                      this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                      this.totalPorcentagemV = ((this.parcela5 / 100) * 100);
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                        
                        this.taxaCliente = 0;
                        this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        //console.log(this.valorParcela);
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = this.taxaParcelamentoAux;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;


                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                      this.taxasAux = valor * ((this.creditoParcelado14 / 100) + 0.002);
                      this.taxaParcelamentoAux = valor * 0;
                      this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                      this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela5 / 100) * 100);
                      this.totalPorVendedor = this.porcentagemVendedor; 

                      this.taxaCliente = valor * ((this.parcela5 / 100) + 0.0037);
                      this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                      this.valorRecebido = this.valorAux + this.taxaCliente;
                      this.qtdParcelas = parcelas;
                      this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                      //console.log(this.valorParcela);
                      this.totalImp = this.taxasAux;
                      this.totalParcelamento = this.taxaParcelamentoAux;
                      this.totalTaxas = this.totalImp + this.totalParcelamento;
                      this.total = this.valorAux - this.totalTaxas;
                      this.totalCliente = this.valorRecebido;


                  } // fim do if do cliente

                }//Fim do if da quantidade de parcelas 5
                else if(parcelas == 6){ //Inicio do if da quantidade de parcelas 6
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                        
                        //this.valorReal = valor;
                      this.taxasAux = valor * ((this.creditoParcelado14 / 100) + 0.0069);
                      this.taxaParcelamentoAux = valor * ((this.parcela6 / 100) + 0.0144);
                        
                      this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                      this.totalPorcentagemV = ((this.parcela6 / 100) * 100);
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                        
                        this.taxaCliente = 0;
                        this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        //console.log(this.valorParcela);
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = this.taxaParcelamentoAux;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;


                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                      this.taxasAux = valor * ((this.creditoParcelado14 / 100) + 0.002);
                      this.taxaParcelamentoAux = valor * 0; 
                      this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                      this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela6 / 100) * 100);
                      this.totalPorVendedor = this.porcentagemVendedor;
                      this.taxaCliente = valor * ((this.parcela6 / 100) + 0.0042);
                      this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                      this.valorRecebido = this.valorAux + this.taxaCliente;
                      this.qtdParcelas = parcelas;
                      this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                      //console.log(this.valorParcela);
                      this.totalImp = this.taxasAux;
                      this.totalParcelamento = this.taxaParcelamentoAux;
                      this.totalTaxas = this.totalImp + this.totalParcelamento;
                      this.total = this.valorAux - this.totalTaxas;
                      this.totalCliente = this.valorRecebido;


                  } // fim do if do cliente

                }//Fim do if da quantidade de parcelas 6
                else if(parcelas == 7){ //Inicio do if da quantidade de parcelas 7
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                        
                        //this.valorReal = valor;
                      this.taxasAux = valor * ((this.creditoParcelado14 / 100) + 0.0076);
                      this.taxaParcelamentoAux = valor * ((this.parcela7 / 100) + 0.018);
                        
                      this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                      this.totalPorcentagemV = ((this.parcela7 / 100) * 100);
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                        
                        this.taxaCliente = 0;
                        this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        //console.log(this.valorParcela);
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = this.taxaParcelamentoAux;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;


                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                      this.taxasAux = valor * ((this.creditoParcelado14 / 100) + 0.002);
                      this.taxaParcelamentoAux = valor * 0;
                      this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                      this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela7 / 100) * 100);
                      this.totalPorVendedor = this.porcentagemVendedor; 

                      this.taxaCliente = valor * ((this.parcela7 / 100) + 0.0047);
                      this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                      this.valorRecebido = this.valorAux + this.taxaCliente;
                      this.qtdParcelas = parcelas;
                      this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                      //console.log(this.valorParcela);
                      this.totalImp = this.taxasAux;
                      this.totalParcelamento = this.taxaParcelamentoAux;
                      this.totalTaxas = this.totalImp + this.totalParcelamento;
                      this.total = this.valorAux - this.totalTaxas;
                      this.totalCliente = this.valorRecebido;


                  } // fim do if do cliente

                }//Fim do if da quantidade de parcelas 7
                else if(parcelas == 8){ //Inicio do if da quantidade de parcelas 8
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                        
                        //this.valorReal = valor;
                      this.taxasAux = valor * ((this.creditoParcelado14 / 100) + 0.0083);
                      this.taxaParcelamentoAux = valor * ((this.parcela8 / 100) + 0.0221);
                        
                      this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                      this.totalPorcentagemV = ((this.parcela8 / 100) * 100);
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                        
                        this.taxaCliente = 0;
                        this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        //console.log(this.valorParcela);
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = this.taxaParcelamentoAux;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;


                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                      this.taxasAux = valor * ((this.creditoParcelado14 / 100) + 0.002);
                      this.taxaParcelamentoAux = valor * 0; 
                      this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                      this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela8 / 100) * 100);
                      this.totalPorVendedor = this.porcentagemVendedor;

                      this.taxaCliente = valor * ((this.parcela8 / 100) + 0.0053);
                      this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                      this.valorRecebido = this.valorAux + this.taxaCliente;
                      this.qtdParcelas = parcelas;
                      this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                      //console.log(this.valorParcela);
                      this.totalImp = this.taxasAux;
                      this.totalParcelamento = this.taxaParcelamentoAux;
                      this.totalTaxas = this.totalImp + this.totalParcelamento;
                      this.total = this.valorAux - this.totalTaxas;
                      this.totalCliente = this.valorRecebido;


                  } // fim do if do cliente

                }//Fim do if da quantidade de parcelas 8
                else if(parcelas == 9){ //Inicio do if da quantidade de parcelas 9
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                        
                        //this.valorReal = valor;
                      this.taxasAux = valor * ((this.creditoParcelado14 / 100) + 0.009);
                      this.taxaParcelamentoAux = valor * ((this.parcela9 / 100) + 0.0264);
                        
                      this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                      this.totalPorcentagemV = ((this.parcela9 / 100) * 100);
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                        
                        this.taxaCliente = 0;
                        this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        //console.log(this.valorParcela);
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = this.taxaParcelamentoAux;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;


                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                      this.taxasAux = valor * ((this.creditoParcelado14 / 100) + 0.002);
                      this.taxaParcelamentoAux = valor * 0;
                      this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                      this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela9 / 100) * 100);
                      this.totalPorVendedor = this.porcentagemVendedor; 

                      this.taxaCliente = valor * ((this.parcela9 / 100) + 0.0059);
                      this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                      this.valorRecebido = this.valorAux + this.taxaCliente;
                      this.qtdParcelas = parcelas;
                      this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                      //console.log(this.valorParcela);
                      this.totalImp = this.taxasAux;
                      this.totalParcelamento = this.taxaParcelamentoAux;
                      this.totalTaxas = this.totalImp + this.totalParcelamento;
                      this.total = this.valorAux - this.totalTaxas;
                      this.totalCliente = this.valorRecebido;


                  } // fim do if do cliente

                }//Fim do if da quantidade de parcelas 9
                else if(parcelas == 10){ //Inicio do if da quantidade de parcelas 10
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                        
                        //this.valorReal = valor;
                      this.taxasAux = valor * ((this.creditoParcelado14 / 100) + 0.0098);
                      this.taxaParcelamentoAux = valor * ((this.parcela10 / 100) + 0.0311);
                        
                      this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                      this.totalPorcentagemV = ((this.parcela10 / 100) * 100);
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                        
                        this.taxaCliente = 0;
                        this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        //console.log(this.valorParcela);
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = this.taxaParcelamentoAux;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;


                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                      this.taxasAux = valor * ((this.creditoParcelado14 / 100) + 0.002);
                      this.taxaParcelamentoAux = valor * 0;
                      this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                      this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela10 / 100) * 100);
                      this.totalPorVendedor = this.porcentagemVendedor; 

                      this.taxaCliente = valor * ((this.parcela10 / 100) + 0.0063);
                      this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                      this.valorRecebido = this.valorAux + this.taxaCliente;
                      this.qtdParcelas = parcelas;
                      this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                      //console.log(this.valorParcela);
                      this.totalImp = this.taxasAux;
                      this.totalParcelamento = this.taxaParcelamentoAux;
                      this.totalTaxas = this.totalImp + this.totalParcelamento;
                      this.total = this.valorAux - this.totalTaxas;
                      this.totalCliente = this.valorRecebido;
                  } // fim do if do cliente
                }//Fim do if da quantidade de parcelas 10
                else if(parcelas == 11){ //Inicio do if da quantidade de parcelas 11
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                        
                        //this.valorReal = valor;
                      this.taxasAux = valor * ((this.creditoParcelado14 / 100) + 0.0105);
                      this.taxaParcelamentoAux = valor * ((this.parcela11 / 100) + 0.0362);
                        
                      this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                      this.totalPorcentagemV = ((this.parcela11 / 100) * 100);
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                        
                        this.taxaCliente = 0;
                        this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        //console.log(this.valorParcela);
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = this.taxaParcelamentoAux;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;


                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                      this.taxasAux = valor * ((this.creditoParcelado14 / 100) + 0.002);
                      this.taxaParcelamentoAux = valor * 0;
                      this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                      this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela11 / 100) * 100);
                      this.totalPorVendedor = this.porcentagemVendedor; 

                      this.taxaCliente = valor * ((this.parcela11 / 100) + 0.0068);
                      this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                      this.valorRecebido = this.valorAux + this.taxaCliente;
                      this.qtdParcelas = parcelas;
                      this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                      //console.log(this.valorParcela);
                      this.totalImp = this.taxasAux;
                      this.totalParcelamento = this.taxaParcelamentoAux;
                      this.totalTaxas = this.totalImp + this.totalParcelamento;
                      this.total = this.valorAux - this.totalTaxas;
                      this.totalCliente = this.valorRecebido;


                  } // fim do if do cliente

                }//Fim do if da quantidade de parcelas 11
                else if(parcelas == 12){ //Inicio do if da quantidade de parcelas 12
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                        
                        //this.valorReal = valor;
                      this.taxasAux = valor * ((this.creditoParcelado14 / 100) + 0.0113);
                      this.taxaParcelamentoAux = valor * ((this.parcela12 / 100) + 0.0417);
                        
                      this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                      this.totalPorcentagemV = ((this.parcela12 / 100) * 100);
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                        
                        this.taxaCliente = 0;
                        this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        //console.log(this.valorParcela);
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = this.taxaParcelamentoAux;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;


                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                      this.taxasAux = valor * ((this.creditoParcelado14 / 100) + 0.002);
                      this.taxaParcelamentoAux = valor * 0;
                      this.porcentagemVendedor = ((this.creditoParcelado14 / 100) * 100);
                      this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela12 / 100) * 100);
                      this.totalPorVendedor = this.porcentagemVendedor; 

                      this.taxaCliente = valor * ((this.parcela12 / 100) + 0.0074);
                      this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                      this.valorRecebido = this.valorAux + this.taxaCliente;
                      this.qtdParcelas = parcelas;
                      this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                      //console.log(this.valorParcela);
                      this.totalImp = this.taxasAux;
                      this.totalParcelamento = this.taxaParcelamentoAux;
                      this.totalTaxas = this.totalImp + this.totalParcelamento;
                      this.total = this.valorAux - this.totalTaxas;
                      this.totalCliente = this.valorRecebido;


                  } // fim do if do cliente

                }//Fim do if da quantidade de parcelas 12
              } //Fim do if do tipo credito em 14 dias
          } // Fim do if do Recebimento em 14 dias
          else if (recebimento == "30") { //Inicio do if do Recebimento em 30 dias
            if (pagamento == "Credito") { // Inicio do if do pagamento do tipo credito
                  if(parcelas == 1){ //Inicio do if da quantidade de parcelas 1
                      if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                        
                        this.taxasAux = valor * ((this.credito30 / 100) + 0.001);
                
                        this.taxaCliente = 0;
                        this.valorAux = valor + this.taxasAux;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        //console.log(this.valorParcela);
                        this.porcentagemVendedor = ((this.credito30/100) * 100);
                        this.totalPorVendedor = this.porcentagemVendedor;
                        this.totalPorCliente = this.taxaCliente;
                        this.totalPorcentagemV = 0;
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = valor * 0;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;

                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                        //Variaveis auxiliar "taxasAux e valorAux" para calcular o reverso
                        this.taxasAux = valor * ((this.credito30 / 100) + 0.001);
                        this.valorAux = valor + this.taxasAux;
                
                        this.taxaCliente = this.valorAux * 0;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        //console.log(this.valorParcela);
                        this.porcentagemVendedor = ((this.credito30 / 100) * 100);
                        this.totalPorVendedor = this.porcentagemVendedor;
                        this.totalPorCliente = this.taxaCliente;
                        this.totalPorcentagemV = 0;
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = valor * 0;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;
                    } // fim do if do cliente

                  }//Fim do if da quantidade de parcelas 1
                  else if(parcelas == 2){ //Inicio do if da quantidade de parcelas 2
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                        
                        //this.valorReal = valor;
                      this.taxasAux = valor * ((this.creditoParcelado30 / 100) + 0.003);
                      this.taxaParcelamentoAux = valor * ((this.parcela2 / 100) + 0.0034);
                        
                      this.porcentagemVendedor = ((this.creditoParcelado30/100) * 100);
                      this.totalPorcentagemV = ((this.parcela2 / 100) * 100);
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                        
                        this.taxaCliente = 0;
                        this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        //console.log(this.valorParcela);
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = this.taxaParcelamentoAux;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;


                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                      this.taxasAux = valor * ((this.creditoParcelado30 / 100) + 0.0014);
                      this.taxaParcelamentoAux = valor * 0;
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                      this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela2 / 100) * 100);
                      this.totalPorVendedor = this.porcentagemVendedor; 

                      this.taxaCliente = valor * ((this.parcela2 / 100) + 0.0016);
                      this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                      this.valorRecebido = this.valorAux + this.taxaCliente;
                      this.qtdParcelas = parcelas;
                      this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                      //console.log(this.valorParcela);
                      this.totalImp = this.taxasAux;
                      this.totalParcelamento = valor * 0;
                      this.totalTaxas = this.totalImp + this.totalParcelamento;
                      this.total = this.valorAux - this.totalTaxas;
                      this.totalCliente = this.valorRecebido;


                  } // fim do if do cliente

                }//Fim do if da quantidade de parcelas 2
                else if(parcelas == 3){ //Inicio do if da quantidade de parcelas 3
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                        
                        //this.valorReal = valor;
                      this.taxasAux = valor * ((this.creditoParcelado30 / 100) + 0.0036);
                      this.taxaParcelamentoAux = valor * ((this.parcela3 / 100) + 0.0054);
                        
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                      this.totalPorcentagemV = ((this.parcela3 / 100) * 100);
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                        
                        this.taxaCliente = 0;
                        this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        //console.log(this.valorParcela);
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = this.taxaParcelamentoAux;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;


                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                      this.taxasAux = valor * ((this.creditoParcelado30 / 100) + 0.0014);
                      this.taxaParcelamentoAux = valor * 0;
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                      this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela3 / 100) * 100);
                      this.totalPorVendedor = this.porcentagemVendedor;   

                      this.taxaCliente = valor * ((this.parcela3 / 100) + 0.002);
                      this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                      this.valorRecebido = this.valorAux + this.taxaCliente;
                      this.qtdParcelas = parcelas;
                      this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                      //console.log(this.valorParcela);
                      this.totalImp = this.taxasAux;
                      this.totalParcelamento = valor * 0;
                      this.totalTaxas = this.totalImp + this.totalParcelamento;
                      this.total = this.valorAux - this.totalTaxas;
                      this.totalCliente = this.valorRecebido;


                  } // fim do if do cliente

                }//Fim do if da quantidade de parcelas 3
                else if(parcelas == 4){ //Inicio do if da quantidade de parcelas 4
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                        
                        //this.valorReal = valor;
                      this.taxasAux = valor * ((this.creditoParcelado30 / 100) + 0.0042);
                      this.taxaParcelamentoAux = valor * ((this.parcela4 / 100) + 0.0206);
                        
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                      this.totalPorcentagemV = ((this.parcela4 / 100) * 100);
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                        
                        this.taxaCliente = 0;
                        this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        //console.log(this.valorParcela);
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = this.taxaParcelamentoAux;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;


                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                      this.taxasAux = valor * ((this.creditoParcelado30 / 100) + 0.0014);
                      this.taxaParcelamentoAux = valor * 0;
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                      this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela4 / 100) * 100);
                      this.totalPorVendedor = this.porcentagemVendedor; 

                      this.taxaCliente = valor * ((this.parcela4 / 100) + 0.0154);
                      this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                      this.valorRecebido = this.valorAux + this.taxaCliente;
                      this.qtdParcelas = parcelas;
                      this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                      //console.log(this.valorParcela);
                      this.totalImp = this.taxasAux;
                      this.totalParcelamento = this.taxaParcelamentoAux;
                      this.totalTaxas = this.totalImp + this.totalParcelamento;
                      this.total = this.valorAux - this.totalTaxas;
                      this.totalCliente = this.valorRecebido;


                  } // fim do if do cliente

                }//Fim do if da quantidade de parcelas 4
                else if(parcelas == 5){ //Inicio do if da quantidade de parcelas 5
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                        
                        //this.valorReal = valor;
                      this.taxasAux = valor * ((this.creditoParcelado30 / 100) + 0.0047);
                      this.taxaParcelamentoAux = valor * ((this.parcela5 / 100) + 0.0104);
                        
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                      this.totalPorcentagemV = ((this.parcela5 / 100) * 100);
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                        
                        this.taxaCliente = 0;
                        this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        //console.log(this.valorParcela);
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = this.taxaParcelamentoAux;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;


                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                      this.taxasAux = valor * ((this.creditoParcelado30 / 100) + 0.0014);
                      this.taxaParcelamentoAux = valor * 0;
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                      this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela5 / 100) * 100);
                      this.totalPorVendedor = this.porcentagemVendedor; 

                      this.taxaCliente = valor * ((this.parcela5 / 100) + 0.003);
                      this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                      this.valorRecebido = this.valorAux + this.taxaCliente;
                      this.qtdParcelas = parcelas;
                      this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                      //console.log(this.valorParcela);
                      this.totalImp = this.taxasAux;
                      this.totalParcelamento = this.taxaParcelamentoAux;
                      this.totalTaxas = this.totalImp + this.totalParcelamento;
                      this.total = this.valorAux - this.totalTaxas;
                      this.totalCliente = this.valorRecebido;


                  } // fim do if do cliente

                }//Fim do if da quantidade de parcelas 5
                else if(parcelas == 6){ //Inicio do if da quantidade de parcelas 6
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                        
                        //this.valorReal = valor;
                      this.taxasAux = valor * ((this.creditoParcelado30 / 100) + 0.0053);
                      this.taxaParcelamentoAux = valor * ((this.parcela6 / 100) + 0.0135);
                        
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                      this.totalPorcentagemV = ((this.parcela6 / 100) * 100);
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                        
                        this.taxaCliente = 0;
                        this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        //console.log(this.valorParcela);
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = this.taxaParcelamentoAux;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;


                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                      this.taxasAux = valor * ((this.creditoParcelado30 / 100) + 0.0014);
                      this.taxaParcelamentoAux = valor * 0;
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                      this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela6 / 100) * 100);
                      this.totalPorVendedor = this.porcentagemVendedor; 

                      this.taxaCliente = valor * ((this.parcela6 / 100) + 0.0034);
                      this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                      this.valorRecebido = this.valorAux + this.taxaCliente;
                      this.qtdParcelas = parcelas;
                      this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                      //console.log(this.valorParcela);
                      this.totalImp = this.taxasAux;
                      this.totalParcelamento = this.taxaParcelamentoAux;
                      this.totalTaxas = this.totalImp + this.totalParcelamento;
                      this.total = this.valorAux - this.totalTaxas;
                      this.totalCliente = this.valorRecebido;


                  } // fim do if do cliente

                }//Fim do if da quantidade de parcelas 6
                else if(parcelas == 7){ //Inicio do if da quantidade de parcelas 7
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                        
                        //this.valorReal = valor;
                      this.taxasAux = valor * ((this.creditoParcelado30 / 100) + 0.0059);
                      this.taxaParcelamentoAux = valor * ((this.parcela7 / 100) + 0.017);
                        
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                      this.totalPorcentagemV = ((this.parcela7 / 100) * 100);
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                        
                        this.taxaCliente = 0;
                        this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        //console.log(this.valorParcela);
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = this.taxaParcelamentoAux;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;


                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                      this.taxasAux = valor * ((this.creditoParcelado30 / 100) + 0.0014);
                      this.taxaParcelamentoAux = valor * 0;
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                      this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela7 / 100) * 100);
                      this.totalPorVendedor = this.porcentagemVendedor; 

                      this.taxaCliente = valor * ((this.parcela7 / 100) + 0.0039);
                      this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                      this.valorRecebido = this.valorAux + this.taxaCliente;
                      this.qtdParcelas = parcelas;
                      this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                      //console.log(this.valorParcela);
                      this.totalImp = this.taxasAux;
                      this.totalParcelamento = this.taxaParcelamentoAux;
                      this.totalTaxas = this.totalImp + this.totalParcelamento;
                      this.total = this.valorAux - this.totalTaxas;
                      this.totalCliente = this.valorRecebido;


                  } // fim do if do cliente

                }//Fim do if da quantidade de parcelas 7
                else if(parcelas == 8){ //Inicio do if da quantidade de parcelas 8
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                        
                        //this.valorReal = valor;
                      this.taxasAux = valor * ((this.creditoParcelado30 / 100) + 0.0065);
                      this.taxaParcelamentoAux = valor * ((this.parcela8 / 100) + 0.0209);
                        
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                      this.totalPorcentagemV = ((this.parcela8 / 100) * 100);
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                        
                        this.taxaCliente = 0;
                        this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        //console.log(this.valorParcela);
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = this.taxaParcelamentoAux;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;


                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                      this.taxasAux = valor * ((this.creditoParcelado30 / 100) + 0.0014);
                      this.taxaParcelamentoAux = valor * 0;
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                      this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela8 / 100) * 100);
                      this.totalPorVendedor = this.porcentagemVendedor; 

                      this.taxaCliente = valor * ((this.parcela8 / 100) + 0.0043);
                      this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                      this.valorRecebido = this.valorAux + this.taxaCliente;
                      this.qtdParcelas = parcelas;
                      this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                      //console.log(this.valorParcela);
                      this.totalImp = this.taxasAux;
                      this.totalParcelamento = this.taxaParcelamentoAux;
                      this.totalTaxas = this.totalImp + this.totalParcelamento;
                      this.total = this.valorAux - this.totalTaxas;
                      this.totalCliente = this.valorRecebido;


                  } // fim do if do cliente

                }//Fim do if da quantidade de parcelas 8
                else if(parcelas == 9){ //Inicio do if da quantidade de parcelas 9
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                        
                        //this.valorReal = valor;
                      this.taxasAux = valor * ((this.creditoParcelado30 / 100) + 0.0071);
                      this.taxaParcelamentoAux = valor * ((this.parcela9 / 100) + 0.025);
                        
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                      this.totalPorcentagemV = ((this.parcela9 / 100) * 100);
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                        
                        this.taxaCliente = 0;
                        this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        //console.log(this.valorParcela);
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = this.taxaParcelamentoAux;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;


                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                      this.taxasAux = valor * ((this.creditoParcelado30 / 100) + 0.0014);
                      this.taxaParcelamentoAux = valor * 0;
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                      this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela9 / 100) * 100);
                      this.totalPorVendedor = this.porcentagemVendedor; 

                      this.taxaCliente = valor * ((this.parcela9 / 100) + 0.0048);
                      this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                      this.valorRecebido = this.valorAux + this.taxaCliente;
                      this.qtdParcelas = parcelas;
                      this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                      //console.log(this.valorParcela);
                      this.totalImp = this.taxasAux;
                      this.totalParcelamento = this.taxaParcelamentoAux;
                      this.totalTaxas = this.totalImp + this.totalParcelamento;
                      this.total = this.valorAux - this.totalTaxas;
                      this.totalCliente = this.valorRecebido;


                  } // fim do if do cliente

                }//Fim do if da quantidade de parcelas 9
                else if(parcelas == 10){ //Inicio do if da quantidade de parcelas 10
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                        
                        //this.valorReal = valor;
                      this.taxasAux = valor * ((this.creditoParcelado30 / 100) + 0.0077);
                      this.taxaParcelamentoAux = valor * ((this.parcela10 / 100) + 0.0296);
                        
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                      this.totalPorcentagemV = ((this.parcela10 / 100) * 100);
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                        
                        this.taxaCliente = 0;
                        this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        //console.log(this.valorParcela);
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = this.taxaParcelamentoAux;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;


                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                      this.taxasAux = valor * ((this.creditoParcelado30 / 100) + 0.0014);
                      this.taxaParcelamentoAux = valor * 0; 

                      this.taxaCliente = valor * ((this.parcela10 / 100) + 0.0052);
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                      this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela10 / 100) * 100);
                      this.totalPorVendedor = this.porcentagemVendedor;
                      this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                      this.valorRecebido = this.valorAux + this.taxaCliente;
                      this.qtdParcelas = parcelas;
                      this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                      //console.log(this.valorParcela);
                      this.totalImp = this.taxasAux;
                      this.totalParcelamento = this.taxaParcelamentoAux;
                      this.totalTaxas = this.totalImp + this.totalParcelamento;
                      this.total = this.valorAux - this.totalTaxas;
                      this.totalCliente = this.valorRecebido;


                  } // fim do if do cliente

                }//Fim do if da quantidade de parcelas 10
                else if(parcelas == 11){ //Inicio do if da quantidade de parcelas 11
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                        
                        //this.valorReal = valor;
                      this.taxasAux = valor * ((this.creditoParcelado30 / 100) + 0.0083);
                      this.taxaParcelamentoAux = valor * ((this.parcela11 / 100) + 0.0345);
                        
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                      this.totalPorcentagemV = ((this.parcela11 / 100) * 100);
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                        
                        this.taxaCliente = 0;
                        this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        //console.log(this.valorParcela);
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = this.taxaParcelamentoAux;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;


                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                      this.taxasAux = valor * ((this.creditoParcelado30 / 100) + 0.0014);
                      this.taxaParcelamentoAux = valor * 0;
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                      this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela11 / 100) * 100);
                      this.totalPorVendedor = this.porcentagemVendedor; 

                      this.taxaCliente = valor * ((this.parcela11 / 100) + 0.0056);
                      this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                      this.valorRecebido = this.valorAux + this.taxaCliente;
                      this.qtdParcelas = parcelas;
                      this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                      //console.log(this.valorParcela);
                      this.totalImp = this.taxasAux;
                      this.totalParcelamento = this.taxaParcelamentoAux;
                      this.totalTaxas = this.totalImp + this.totalParcelamento;
                      this.total = this.valorAux - this.totalTaxas;
                      this.totalCliente = this.valorRecebido;


                  } // fim do if do cliente

                }//Fim do if da quantidade de parcelas 11
                else if(parcelas == 12){ //Inicio do if da quantidade de parcelas 12
                    if (parcelamento == "Vendedor") { //Inicio do if do Vendedor
                        
                        //this.valorReal = valor;
                      this.taxasAux = valor * ((this.creditoParcelado30 / 100) + 0.0089);
                      this.taxaParcelamentoAux = valor * ((this.parcela12 / 100) + 0.0397);
                        
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                      this.totalPorcentagemV = ((this.parcela12 / 100) * 100);
                        this.totalPorCliente = 0;
                        this.totalPorVendedor = this.porcentagemVendedor + this.totalPorcentagemV;
                        
                        this.taxaCliente = 0;
                        this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                        this.qtdParcelas = parcelas;
                        this.valorParcela = (this.valorAux/this.qtdParcelas);
                        //console.log(this.valorParcela);
                        this.totalImp = this.taxasAux;
                        this.totalParcelamento = this.taxaParcelamentoAux;
                        this.totalTaxas = this.totalImp + this.totalParcelamento;
                        this.total = this.valorAux - this.totalTaxas;
                        this.totalCliente = this.valorAux;


                  //Fim do if do Vendedor 
                  } else if(parcelamento =="Cliente"){ //Inicio do if do cliente
                    //Calculos
                      this.taxasAux = valor * ((this.creditoParcelado30 / 100) + 0.0014);
                      this.taxaParcelamentoAux = valor * 0;
                      this.porcentagemVendedor = ((this.creditoParcelado30 / 100) * 100);
                      this.totalPorcentagemV = 0;
                      this.totalPorCliente = ((this.parcela12 / 100) * 100);
                      this.totalPorVendedor = this.porcentagemVendedor; 

                      this.taxaCliente = valor * ((this.parcela12 / 100) + 0.006);
                      this.valorAux = valor + this.taxasAux + this.taxaParcelamentoAux;
                      this.valorRecebido = this.valorAux + this.taxaCliente;
                      this.qtdParcelas = parcelas;
                      this.valorParcela = (this.valorRecebido/this.qtdParcelas);
                      //console.log(this.valorParcela);
                      this.totalImp = this.taxasAux;
                      this.totalParcelamento = this.taxaParcelamentoAux;
                      this.totalTaxas = this.totalImp + this.totalParcelamento;
                      this.total = this.valorAux - this.totalTaxas;
                      this.totalCliente = this.valorRecebido;
                  } // fim do if do cliente
                }//Fim do if da quantidade de parcelas 12
              }//Fim do if do pagamento do tipo credito
          }//Fim do if do Recebimento em 30 dias
        }

      } 
  }

 reset() {
    var self = this;
    setTimeout(function(){ 
      self.state = false;
    }, 1000);
  }

  screenShot() {
    this.screenshot.save('jpg', 80).then(res => {
      this.screen = res.filePath;
      this.state = true;
      this.reset();
    });
  }																																																																																																																																																																																																																																																																																																																																																																																				

  ionViewWillLoad() {
    //const data = this.navParams.get('data');
    //console.log(data);
  }

  closeModal(){
  	this.view.dismiss();
  }



}
