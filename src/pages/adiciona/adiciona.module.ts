import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdicionaPage } from './adiciona';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    AdicionaPage,
  ],
  imports: [
    TranslateModule,
    IonicPageModule.forChild(AdicionaPage),
  ],
})
export class AdicionaPageModule {}
